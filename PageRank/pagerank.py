# solutions.py
"""Volume 1: The Page Rank Algorithm.
<Name>
<Class>
<Date>
"""
import numpy as np
from numpy import linalg as la
import pandas as pd
import networkx as nx
import time

# Problems 1-2
class DiGraph:
    """A class for representing directed graphs via their adjacency matrices.

    Attributes:
        (fill this out after completing DiGraph.__init__().)
    """
    # Problem 1
    def __init__(self, A, labels=None):
        """Modify A so that there are no sinks in the corresponding graph,
        then calculate Ahat. Save Ahat and the labels as attributes.

        Parameters:
            A ((n,n) ndarray): the adjacency matrix of a directed graph.
                A[i,j] is the weight of the edge from node j to node i.
            labels (list(str)): labels for the n nodes in the graph.
                If None, defaults to [0, 1, ..., n-1].
        """

        M, N = A.shape
        if labels == None:
            labels = np.arange(N)
            labels = [str(i) for i in labels]
        else: 
            if len(labels) != N:
                raise ValueError("Number of labels not equal to number of nodes")
        Ahat = np.zeros(A.shape)
        for n in range(N):
            if np.all(A[:,n] == 0):
                A[:,n] = np.ones(N)

            Ahat[:,n] = A[:,n]/(A[:,n].sum())
        self.Ahat = Ahat
        self.labels = labels
        self.n = N

    # Problem 2
    def linsolve(self, epsilon=0.85):
        """Compute the PageRank vector using the linear system method.

        Parameters:
            epsilon (float): the  damping factor, between 0 and 1.

        Returns:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        n = self.n
        right = (1-epsilon)*np.ones(n)/n
        left = np.eye(n)-(epsilon*self.Ahat)
        vals = la.solve(left, right)
        p = dict(zip(self.labels,vals))
        return p
    
    
    # Problem 2
    def eigensolve(self, epsilon=0.85):
        """Compute the PageRank vector using the eigenvalue method.
        Normalize the resulting eigenvector so its entries sum to 1.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """
        n = self.n
        E = np.array([np.ones(n) for _ in range(n)]).reshape((n,n))
        B = epsilon*self.Ahat + (1-epsilon)*E/n
        p = la.eig(B)[1][:,0]
        return dict(zip(self.labels,p/p.sum()))
        
    # Problem 2
    def itersolve(self, epsilon=0.85, maxiter=100, tol=1e-12):
        """Compute the PageRank vector using the iterative method.

        Parameters:
            epsilon (float): the damping factor, between 0 and 1.
            maxiter (int): the maximum number of iterations to compute.
            tol (float): the convergence tolerance.

        Return:
            dict(str -> float): A dictionary mapping labels to PageRank values.
        """     
        n = self.n
        p = np.ones(n)/n
        Ahat = self.Ahat
        one = np.ones(n)
        for _ in range(maxiter):
            pnext = epsilon*Ahat@p+(1-epsilon)*one/n
            if la.norm(pnext - p, ord = 1) < tol:
                break
            p = pnext
        return dict(zip(self.labels,pnext))
        
        
        
# Problem 3
def get_ranks(d):
    """Construct a sorted list of labels based on the PageRank vector.

    Parameters:
        d (dict(str -> float)): a dictionary mapping labels to PageRank values.

    Returns:
        (list) the keys of d, sorted by PageRank value from greatest to least.
    """
    s = sorted([(-val,key) for key, val in zip(d.keys(),d.values())])
    return [pair[1] for pair in s]


# Problem 4
def rank_websites(filename="web_stanford.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i if webpage j has a hyperlink to webpage i. Use the DiGraph class
    and its itersolve() method to compute the PageRank values of the webpages,
    then rank them with get_ranks(). If two webpages have the same rank,
    resolve ties by listing the webpage with the larger ID number first.

    Each line of the file has the format
        a/b/c/d/e/f...
    meaning the webpage with ID 'a' has hyperlinks to the webpages with IDs
    'b', 'c', 'd', and so on.

    Parameters:
        filename (str): the file to read from.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of webpage IDs.
    """
    while True:
           try:
               with open(filename,encoding='utf-8') as infile:
                   data=infile.read()
               break  
           except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
               infile=input("Please input a valid filename:")
    contents = data.strip('\n').split('\n')
    
    wwweb = []
    LabelsPrep = []
    for line in contents:   #iterate through each line of the contents
        labels_prep = line.strip('/').split('/') #separate each element
        wwweb.append(labels_prep)   #We will use wwweb to build our adjacency
        for element in labels_prep: #Check if each element of the list is in our list
            if element not in LabelsPrep:
                LabelsPrep.append(element)

    Labels = sorted(list(set(LabelsPrep))) #Make sure Labels is nice and clean
    Labeldict = {}
    for i in range(len(Labels)):    #Prepare labels index dictionary
        Labeldict[Labels[i]] = i    #The ith Label will have a label i
    
    A = np.zeros((len(Labels),len(Labels)))  #empty zero matrix
    for line in wwweb:                       #iterate through each web and its links
        j = Labeldict[line[0]]               #column value is the page's index
        for item in line[1:]:                #row value is the link's index
            i = Labeldict[item] 
            A[i,j]=1                         #create adjacency
    
    webthingy = DiGraph(A, Labels)
    d = webthingy.itersolve(epsilon=epsilon)
    return get_ranks(d)
    
    
# Problem 5
def rank_ncaa_teams(filename, epsilon=0.85):
    """Read the specified file and construct a graph where node j points to
    node i with weight w if team j was defeated by team i in w games. Use the
    DiGraph class and its itersolve() method to compute the PageRank values of
    the teams, then rank them with get_ranks().

    Each line of the file has the format
        A,B
    meaning team A defeated team B.

    Parameters:
        filename (str): the name of the data file to read.
        epsilon (float): the damping factor, between 0 and 1.

    Returns:
        (list(str)): The ranked list of team names.
    """
    games = pd.read_csv(filename)
    teams = list(set(games.Winner.values))
    losers = list(set(games.Loser.values))
    for t in losers:
        if t not in teams:
            teams = np.append(teams,t)
    teams = sorted(list(set(teams)))
    teamindex = {}
    for i in range(len(teams)):
        teamindex[teams[i]] = i
    n = len(teams)
    A = np.zeros((n,n))
    for ind in range(len(games)):
        j = teamindex[games.loc[ind]['Loser']]
        i = teamindex[games.loc[ind]['Winner']]
        A[i,j]+=1
    
    # BYU = A[:,teamindex['BYU']]
    # for k in range(len(BYU)):
    #     if BYU[k] >= 1:
    #         print(teams[k])
            
    ncaa = DiGraph(A, teams)
    d = ncaa.itersolve(epsilon)
    return get_ranks(d)

# Problem 6
def rank_actors(filename="top250movies.txt", epsilon=0.85):
    """Read the specified file and construct a graph where node a points to
    node b with weight w if actor a and actor b were in w movies together but
    actor b was listed first. Use NetworkX to compute the PageRank values of
    the actors, then rank them with get_ranks().

    Each line of the file has the format
        title/actor1/actor2/actor3/...
    meaning actor2 and actor3 should each have an edge pointing to actor1,
    and actor3 should have an edge pointing to actor2.
    """
    
    DG = nx.DiGraph()
    
    while True:
           try:
               with open(filename,encoding='utf-8') as infile:
                   data=infile.read()
               break  
           except (FileNotFoundError, FileExistsError, IOError, SyntaxError):
               infile=input("Please input a valid filename:")
    contents = data.strip('\n').split('\n')
    
    actors = []
    LabelsPrep = []
    for line in contents:
        labels_prep = line.strip('/').split('/')
        actors.append(labels_prep)      #create list of lists with each movie
        for element in labels_prep[1:]: #add actor names to list
            if element not in LabelsPrep:
                LabelsPrep.append(element)
    
    Labels = sorted(list(set(LabelsPrep)))
    DG.add_nodes_from(Labels)           #create DiGraph of actors
    for movie in actors:                #iterate through each line/movie
        for i in range(1,len(movie)):   #start with the first actor
            for j in range(len(movie)-1, i, -1): #iterate backwards from last
                                                 #actor to ith actor
                if not DG.has_edge(movie[j], movie[i]): #if there is no edge
                                                        #between actor j and 
                                                        #actor i
                    DG.add_edge(movie[j], movie[i], weight = 1) #add edge
                DG[movie[j]][movie[i]]['weight'] += 1   #adjust weight
    
    return get_ranks(nx.pagerank(DG, alpha = epsilon))
    

def choose_this(left, right, ranks):
    if int(ranks.index(left)) > int(ranks.index(right)):
        return 'choose '+str(right)
    else:
        return 'choose '+str(left)
    
def _adjacency(n, weighted=False):
        """Generate an n x n adjacency matrix with at least 1 sink."""
        A = np.random.randint(0, 2 if not weighted else 5, size=(n,n))
        A[:,np.random.randint(0, n)] = 0
        return A

if __name__ == '__main__':
    # A = np.array([0,0,0,0,1,0,1,0,1,0,0,1,1,0,1,0]).reshape((4,4))
    # labels = ['a','b','c','d']
    # DG = DiGraph(A,labels)
    # d = DG.eigensolve()
    # print(d)
    # print(print(sum(list(d.values()))))
    # print(sum(list(d.values())))
    # print(DG.itersolve())
    # d = DG.itersolve()
    # print(get_ranks(d))
    # print(rank_websites()[:20])
    # print('epsilon = .58', rank_websites(epsilon = .58)[:20])
    # eps65 = rank_ncaa_teams('ncaa2021.csv', epsilon=.65)
    eps85 = rank_ncaa_teams('ncaa2021.csv', epsilon=.95)
    # print(eps85)
    # print(rank_ncaa_teams_years())
    # print(rank_actors())
    # start = time.perf_counter()
    # rank_actors()
    # print(time.perf_counter()-start)
    pass