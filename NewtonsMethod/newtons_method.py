# newtons_method.py
"""Volume 1: Newton's Method.
<Broderik Craig>
<Math 343>
<Date>
"""

import numpy as np
import sympy as sy
from matplotlib import pyplot as plt
from autograd import grad
from autograd import numpy as anp
import numpy.linalg as la
from autograd import jacobian
from scipy.optimize import newton as nwtn

# Problems 1, 3, and 5
def newton(f, x0, Df, tol=1e-5, maxiter=15, alpha=1.):
    """Use Newton's method to approximate a zero of the function f.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.
        alpha (float): Backtracking scalar (Problem 3).

    Returns:
        (float or ndarray): The approximation for a zero of f.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    
    if np.isscalar(x0): #check if the point is a constant
        boo = False
        i = 0
        for _ in range(maxiter):
            xnext = x0 - alpha*f(x0)/Df(x0) #newton's method
            if abs(xnext - x0) < tol: #check if we're within error tolerance
                boo = True
                i += 1
                break
            x0 = xnext
            i+=1
    
    else:
        boo = False
        i = 0
        for _ in range(maxiter):
            xnext = x0 - alpha*la.solve(Df(x0), f(x0))  #same but with vectors
            if la.norm(xnext - x0) < tol:
                boo = True
                i += 1
                break
            x0 = xnext
            i+=1
    return xnext, boo, i

# Problem 2
def prob2(N1, N2, P1, P2):
    """Use Newton's method to solve for the constant r that satisfies

                P1[(1+r)**N1 - 1] = P2[1 - (1+r)**(-N2)].

    Use r_0 = 0.1 for the initial guess.

    Parameters:
        P1 (float): Amount of money deposited into account at the beginning of
            years 1, 2, ..., N1.
        P2 (float): Amount of money withdrawn at the beginning of years N1+1,
            N1+2, ..., N1+N2.
        N1 (int): Number of years money is deposited.
        N2 (int): Number of years money is withdrawn.

    Returns:
        (float): the value of r that satisfies the equation.
    """
    
    f = lambda r: -P1*((1+r)**N1 - 1) + P2*(1 - (1+r)**(-N2)) #set equal to zero
    return newton(f, .1, grad(f), maxiter = 200)[0]

# Problem 4
def optimal_alpha(f, x0, Df, tol=1e-5, maxiter=15):
    """Run Newton's method for various values of alpha in (0,1].
    Plot the alpha value against the number of iterations until convergence.

    Parameters:
        f (function): a function from R^n to R^n (assume n=1 until Problem 5).
        x0 (float or ndarray): The initial guess for the zero of f.
        Df (function): The derivative of f, a function from R^n to R^(nxn).
        tol (float): Convergence tolerance. The function should returns when
            the difference between successive approximations is less than tol.
        maxiter (int): The maximum number of iterations to compute.

    Returns:
        (float): a value for alpha that results in the lowest number of
            iterations.
    """
    
    Alphs = []   #save alpha values
    iterations = [] #save corresponding iteration number
    if np.isscalar(x0):
        for alpha in anp.linspace(.5,1,20):
            boo = False
            i = 0
            for _ in range(maxiter):
                xnext = x0 - alpha*f(x0)/Df(x0)
                if abs(xnext - x0) < tol:
                    boo = True
                    i += 1
                    iterations.append(i)  #add iteration level
                    Alphs.append(alpha)   #add alpha value
                    break
                x0 = xnext
                i+=1
            if boo == False:
                iterations.append(i)     #store last iteration number
                Alphs.append(alpha)
                
    else:  #same but for vectors
        for alpha in anp.linspace(.1,1,20):
            boo = False
            i = 0
            for _ in range(maxiter):
                xnext = x0 - alpha*la.solve(Df(x0), f(x0))
                if la.norm(xnext - x0, ord = len(x0)) < tol:
                    boo = True
                    i += 1
                    iterations.append(i)
                    Alphs.append(alpha)
                    break
                x0 = xnext
                i+=1
            if boo == False:
                iterations.append(i)
                Alphs.append(alpha)
                
    return Alphs[np.argmin(iterations)]


# Problem 6
def prob6():
    """Consider the following Bioremediation system.

                              5xy − x(1 + y) = 0
                        −xy + (1 − y)(1 + y) = 0

    Find an initial point such that Newton’s method converges to either
    (0,1) or (0,−1) with alpha = 1, and to (3.75, .25) with alpha = 0.55.
    Return the intial point as a 1-D NumPy array with 2 entries.
    """
    f = lambda x : anp.array((5*x[0]*x[1] - x[0]*(1+x[1]), -x[0]*x[1] + (1 - x[1])*(1 + x[1])))
    Df = jacobian(f)
    xrange = anp.linspace(-1/4,0)
    yrange = anp.linspace(0,1/4)
    solution1 = anp.array([0,1])
    solution2 = anp.array([0,-1])
    solution3 = anp.array([3.75, .25])
    for i in xrange:    #I hate nested for loops
        for j in yrange:
            x0 = anp.array((i,j))
            if np.allclose(solution1, newton(f, x0, Df)[0]) or np.allclose(solution2, newton(f, x0, Df)[0]):
                if np.allclose(solution3, newton(f,x0, Df, alpha = .55)[0]):
                    return (i,j)


# Problem 7
def plot_basins(f, Df, zeros, domain, res=1000, iters=15):
    """Plot the basins of attraction of f on the complex plane.

    Parameters:
        f (function): A function from C to C.
        Df (function): The derivative of f, a function from C to C.
        zeros (ndarray): A 1-D array of the zeros of f.
        domain ([r_min, r_max, i_min, i_max]): A list of scalars that define
            the window limits and grid domain for the plot.
        res (int): A scalar that determines the resolution of the plot.
            The visualized grid has shape (res, res).
        iters (int): The exact number of times to iterate Newton's method.
    """
    x_real = np.linspace(domain[0], domain[1], res) # Real parts.
    x_imag = np.linspace(domain[2], domain[3], res) # Fake parts.
    X_real, X_imag = np.meshgrid(x_real, x_imag)
    X_0 = X_real + 1j*X_imag
    for k in range(iters):
        Xk = X_0 - (f(X_0)/Df(X_0))
        X_0 = Xk
    Yk = np.empty((res,res))
    for i in range(res):
        for j in range(res):
            Yk[i,j] = np.argmin(abs(zeros - Xk[i][j])) #Store zero values
    
    plt.pcolormesh(x_real, x_imag, Yk, cmap = 'brg')
    
    
    
    
    
    
    
    
    
    
    
    
    