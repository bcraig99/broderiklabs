# drazin.py
"""Volume 1: The Drazin Inverse.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la
import pandas as pd
from scipy.sparse import csgraph

# Helper function for problems 1 and 2.
def index(A, tol=1e-5):
    """Compute the index of the matrix A.

    Parameters:
        A ((n,n) ndarray): An nxn matrix.

    Returns:
        k (int): The index of A.
    """

    # test for non-singularity
    if not np.isclose(la.det(A), 0):
        return 0

    n = len(A)
    k = 1
    Ak = A.copy()
    while k <= n:
        r1 = np.linalg.matrix_rank(Ak)
        r2 = np.linalg.matrix_rank(np.dot(A,Ak))
        if r1 == r2:
            return k
        Ak = np.dot(A,Ak)
        k += 1

    return k


# Problem 1
def is_drazin(A, Ad, k):
    """Verify that a matrix Ad is the Drazin inverse of A.

    Parameters:
        A ((n,n) ndarray): An nxn matrix.
        Ad ((n,n) ndarray): A candidate for the Drazin inverse of A.
        k (int): The index of A.

    Returns:
        (bool) True of Ad is the Drazin inverse of A, False otherwise.
    """
    if np.allclose(A@Ad,Ad@A) and np.allclose(np.linalg.matrix_power(A,k+1)@Ad,np.linalg.matrix_power(A,k)) and np.allclose(Ad@A@Ad,Ad):
        return True
    else:
        return False


# Problem 2
def drazin_inverse(A, tol=1e-4):
    """Compute the Drazin inverse of A.

    Parameters:
        A ((n,n) ndarray): An nxn matrix.

    Returns:
       ((n,n) ndarray) The Drazin inverse of A.
    """
    n,n = A.shape
    T1, Q1, k1 = la.schur(A, sort = lambda x: abs(x) > tol)
    T2, Q2, k2 = la.schur(A, sort = lambda x: abs(x) <= tol)
    U = np.hstack((Q1[:,:k1],Q2[:,:n-k1]))
    Uinv = np.linalg.inv(U)
    V = Uinv @ A @ U
    Z = np.zeros((n,n))
    if k1 != 0:
        Minv = np.linalg.inv(V[:k1,:k1])
        Z[:k1,:k1] = Minv
    
    return U@Z@Uinv

        
# Problem 3
def effective_resistance(A):
    """Compute the effective resistance for each node in a graph.

    Parameters:
        A ((n,n) ndarray): The adjacency matrix of an undirected graph.

    Returns:
        ((n,n) ndarray) The matrix where the ijth entry is the effective
        resistance from node i to node j.
    """
    n,n = A.shape
    L = csgraph.laplacian(A)
       
    R = np.zeros((n,n))
    for j in range(n):
        I = np.eye(n)
        L_j = L.copy()
        L_j[:,j] = I[:,j].copy()
        Ltil = drazin_inverse(L_j)
        R[:,j] = np.diag(Ltil)
        R[j,j] = 0
    return R

# Problems 4 and 5
class LinkPredictor:
    """Predict links between nodes of a network."""

    def __init__(self, filename='social_network.csv'):
        """Create the effective resistance matrix by constructing
        an adjacency matrix.

        Parameters:
            filename (str): The name of a file containing graph data.
        """
        
        data = pd.read_csv(filename, header = None, names = ['col1', 'col2'])

        names = data['col1']
        names = names.append(data['col2'])
        names = np.sort(list(set(names)))
        nameindex = {}
        indexname = {}
        for i, name in enumerate(names):
            nameindex[name] = i
            indexname[i] = name
        n = len(names)
        adjacency = np.zeros((n,n)) 
        for row in range(len(data)):
            crrnt = data.loc[row]
            adjacency[nameindex[crrnt[0]],nameindex[crrnt[1]]] += 1
            adjacency[nameindex[crrnt[1]],nameindex[crrnt[0]]] += 1

        print(np.allclose(adjacency[0],adjacency[:,0]))
        self.resmat = effective_resistance(adjacency)
        self.namesindex = nameindex
        self.names = indexname
        self.nameslist = names
        self.adjacency = adjacency

    def predict_link(self, node=None):
        """Predict the next link, either for the whole graph or for a
        particular node.

        Parameters:
            node (str): The name of a node in the network.

        Returns:
            node1, node2 (str): The names of the next nodes to be linked.
                Returned if node is None.
            node1 (str): The name of the next node to be linked to 'node'.
                Returned if node is not None.

        Raises:
            ValueError: If node is not in the graph.
        """
        if node == None:
            n = len(self.names)
            restemp = []
            for i in range(n):
                row = self.adjacency[i].copy()
                for j, item in enumerate(row):
                    if item == 1:
                        row[j] = 0
                    else:
                        row[j] = 1
                resrow = self.resmat[i].copy() * row
                restemp.append(resrow)
            restemp = np.array(restemp).reshape(n,n)
            minval = np.min(restemp[np.nonzero(restemp)])
            loc = np.where(restemp == minval)
            return self.names[loc[0][0]], self.names[loc[1][0]]
        
        elif node not in self.nameslist:
            raise ValueError(node + ' is not in the graph')
        
        else:
            k = self.namesindex[node]
            row = self.adjacency[:,k].copy()
            for j, item in enumerate(row):
                    if item == 1:
                        row[j] = 0
                    else:
                        row[j] = 1
            resrow = self.resmat[:,k].copy() * row
            minval = np.min(resrow[np.nonzero(resrow)])
            loc = np.where(resrow == minval)
            return self.names[loc[0][0]].copy()

    def add_link(self, node1, node2):
        """Add a link to the graph between node 1 and node 2 by updating the
        adjacency matrix and the effective resistance matrix.

        Parameters:
            node1 (str): The name of a node in the network.
            node2 (str): The name of a node in the network.

        Raises:
            ValueError: If either node1 or node2 is not in the graph.
        """
        
        if node1 not in self.nameslist:
            raise ValueError(node1 + " is not in the graph")
        
        if node2 not in self.nameslist:
            raise ValueError(node2 + " is not in the graph")
        k = self.namesindex[node1]
        l = self.namesindex[node2]
        self.adjacency[k,l] = 1
        self.adjacency[l,k] = 1
        self.resmat = effective_resistance(self.adjacency)
        
        

# if __name__ == "__main__":
#     LP = LinkPredictor()
#     print(LP.predict_link('Alan'))
#     LP.add_link('Alan', 'Sonia')
#     print(LP.predict_link('Alan'))
#     LP.add_link('Alan', LP.predict_link('Alan'))
#     print(LP.predict_link('Alan'))
    # LP.add_link('Alan', LP.predict_link('Alan'))