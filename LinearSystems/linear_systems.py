# linear_systems.py
"""Volume 1: Linear Systems.
<Name>
<Class>
<Date>
"""
import numpy as np
import scipy.linalg as la
import time
from matplotlib import pyplot as plt
from scipy import sparse

# Problem 1
def ref(A):
    """Reduce the square matrix A to REF. You may assume that A is invertible
    and that a 0 will never appear on the main diagonal. Avoid operating on
    entries that you know will be 0 before and after a row operation.

    Parameters:
        A ((n,n) ndarray): The square invertible matrix to be reduced.

    Returns:
        ((n,n) ndarray): The REF of A.
    """
    # for i in range(len(A)):
    #     A[i]=A[i]/A[i,i]
    # A[0]=A[0]/A[0,0]
    for col in range(len(A)):
        for row in range(col+1,len(A)):
            A[row,col:]-=(A[row,col]/A[col,col])*A[col,col:]
    return(A)

def test_ref():
    A=np.random.randint(10,size=(4,4))
    A=A.astype(float)
    A+=np.eye(4)
    # print("A",A,'\n')
    # print("REF \n",ref(A),'\n')
    print("L \n",lu(A)[0],'\n',"U\n",lu(A)[1])
    
    
# Problem 2
def lu(A):
    """Compute the LU decomposition of the square matrix A. You may
    assume that the decomposition exists and requires no row swaps.

    Parameters:
        A ((n,n) ndarray): The matrix to decompose.

    Returns:
        L ((n,n) ndarray): The lower-triangular part of the decomposition.
        U ((n,n) ndarray): The upper-triangular part of the decomposition.
    """
    (m,n)=A.shape
    U=A.copy()
    L=np.eye(m)
    for j in range(n):
        for i in range(j+1,m): 
            L[i,j]=U[i,j]/U[j,j]
            U[i,j:]=U[i,j:]-L[i,j]*U[j,j:]
    return L, U


    # for i in range(n):
    #     # for j in range(i+1,m):
    #         # L[j,i]=U[j,i]/U[i,i]
    #         # U[j]=(U[j]-L[j,i]*U[i])
    #         # # U[i,j]=U[i,j]-L[i,j]*U[j,j]
    #     factor = U[i+1:, i] / U[i, i]
    #     L[i+1:, i] = factor
    #     U[i+1:] -= factor[:, np.newaxis] * U[i]


# Problem 3
def solve(A, b):
    """Use the LU decomposition and back substitution to solve the linear
    system Ax = b. You may again assume that no row swaps are required.

    Parameters:
        A ((n,n) ndarray)
        b ((n,) ndarray)

    Returns:
        x ((m,) ndarray): The solution to the linear system.
    """
    L,U=lu(A)[0],lu(A)[1]
    # print(L,'\n',U,'\n')
    # print(L,U)
    (m,n)=A.shape
    y=np.zeros(n)
    x=np.zeros(n)
    
    for i in range(m):
        y[i]=b[i]
        # print('i',i)
        Sum = 0
        for j in range(i):
            # print('j',j)
            # print((i,j))
            y[i]-=(L[i,j]*y[j])
            # Sum = Sum + L[i,j]*y[j]
            
    # print(y)
    k = n-1
    while k>=0:
        Sum = 0
        j = n-1
        while j >= k+1:
            Sum = Sum + U[k,j]*x[j]
            j-=1
        
        x[k] = (1/U[k,k]) * (y[k] - Sum)
        k-=1
    # return x, la.solve(A,b)
            
    # print(A,'\n',b,x)
    return "This is dumb"



def testsolve():
    A=np.random.randint(10,size=(3,3))
    # A=np.array([7,6,2,5,5,7,1,0,4]).reshape(3,3)
    I=np.eye(3)
    b=np.random.randint(10,size=(3,1))
    return solve(A,b)
    
        # for i in range(n-1,-1,-1):
        # # print('i',i)
        # x[i]=y[i]/U[i,i]
        # # print('x',x[i])
        # for j in range(i+1,n):
        #     # print('j',j)
        #     x[i]-=((U[i,j]*x[j])/U[i,i]

# Problem 4
def prob4():
    """Time different scipy.linalg functions for solving square linear systems.

    For various values of n, generate a random nxn matrix A and a random
    n-vector b using np.random.random(). Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Invert A with la.inv() and left-multiply the inverse to b.
        2. Use la.solve().
        3. Use la.lu_factor() and la.lu_solve() to solve the system with the
            LU decomposition.
        4. Use la.lu_factor() and la.lu_solve(), but only time la.lu_solve()
            (not the time it takes to do the factorization).

    Plot the system size n versus the execution times. Use log scales if
    needed.
    """
    
    domain=2**np.arange(1,14)
    invtime=[]
    solvetime=[]
    factortime=[]
    xval=[]
    startall=time.perf_counter()
    for n in domain:
        A=np.random.randint(10,size=(n,n))
        b=np.random.randint(10,size=(n,1))
        start=time.perf_counter()
        la.inv(A)@b
        end=time.perf_counter()
        invtime.append(end-start)
        
        start=time.perf_counter()
        la.solve(A,b)
        end=time.perf_counter()
        solvetime.append(end-start)
        
        start=time.perf_counter()
        la.lu_solve(la.lu_factor(A),b)
        end=time.perf_counter()
        factortime.append(end-start)
        
        xval.append(n)
        
        print(time.perf_counter()-startall)
        if time.perf_counter()-startall>60:
            break
        
    plt.loglog(xval,invtime,'b.-',label="la.inv(A)*b")
    plt.loglog(xval,solvetime,'r.-',label="la.solve(A,b)")
    plt.loglog(xval,factortime,'g.-',label="la.lu_solve(la.lu_factor(A),b)")
    plt.legend(loc="upper left")
    plt.show()

# Problem 5
def prob5(n):
    """Let I be the n × n identity matrix, and define
                    [B I        ]        [-4  1            ]
                    [I B I      ]        [ 1 -4  1         ]
                A = [  I . .    ]    B = [    1  .  .      ],
                    [      . . I]        [          .  .  1]
                    [        I B]        [             1 -4]
    where A is (n**2,n**2) and each block B is (n,n).
    Construct and returns A as a sparse matrix.

    Parameters:
        n (int): Dimensions of the sparse matrix B.

    Returns:
        A ((n**2,n**2) SciPy sparse matrix)
    """
    I=np.eye(n)
    B=np.zeros(shape=(3,3))
    for i in range(n):
        if i!=0:
            B[i-1,i]=1
        B[i,i]=-4
        if i!=n-1:
            B[i+1,i]=1
            
    M=sparse.bmat(np.array([None]*n**2).reshape(n,n), format='bsr')
    for i in range(n):
        if i!=0:
            M[i-1,i]=I
        M[i,i]=B
        if i!=n-1:
            M[i+1,i]=I

    # A=sparse.bmat(M, format='bsr')
    return(M)





# Problem 6
def prob6():
    """Time regular and sparse linear system solvers.

    For various values of n, generate the (n**2,n**2) matrix A described of
    prob5() and vector b of length n**2. Time how long it takes to solve the
    system Ax = b with each of the following approaches:

        1. Convert A to CSR format and use scipy.sparse.linalg.spsolve()
        2. Convert A to a NumPy array and use scipy.linalg.solve().

    In each experiment, only time how long it takes to solve the system (not
    how long it takes to convert A to the appropriate format). Plot the system
    size n**2 versus the execution times. As always, use log scales where
    appropriate and use a legend to label each line.
    """
    raise NotImplementedError("Problem 6 Incomplete")

# if __name__=="__main__":
    # A=np.random.randint(10,size=(4,4))
    # A=A.astype(float)
    # A+=np.eye(4)
    # print(A)
    # ref(A)
