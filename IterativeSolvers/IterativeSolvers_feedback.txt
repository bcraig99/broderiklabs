03/11/22 13:31

iterative_solvers.py has not been modified yet

-------------------------------------------------------------------------------

03/17/22 17:37

Problem 1 (10 points):
Score += 10

Problem 2 (5 points):
Nothing plotted
Score += 0

Problem 3 (10 points):
Should decrease exactly linearly
Score += 7

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
NotImplementedError: Problem 5 Incomplete

Problem 6 (5 points):
NotImplementedError: Problem 6 Incomplete

Problem 7 (5 points):
NotImplementedError: Problem 7 Incomplete

Code Quality (5 points):
Score += 5

Total score: 27/50 = 54.0%


Comments:
	Keep it up!

-------------------------------------------------------------------------------

03/23/22 09:32

Problem 1 (10 points):
Score += 10

Problem 2 (5 points):
Score += 5

Problem 3 (10 points):
Score += 10

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
Score += 5

Problem 6 (5 points):
Score += 5

Problem 7 (5 points):
Score += 5

Code Quality (5 points):
Score += 5

Total score: 50/50 = 100.0%

Excellent!


Comments:
	Excellent!

-------------------------------------------------------------------------------

