# iterative_solvers.py
"""Volume 1: Iterative Solvers.
<Name>
<Class>
<Date>
"""

import numpy as np
from numpy import linalg as la
from matplotlib import pyplot as plt
from scipy import sparse
# Helper function

def diag_dom(n, num_entries=None, as_sparse=False):
    """Generate a strictly diagonally dominant (n, n) matrix.
    Parameters:
        n (int): The dimension of the system.
        num_entries (int): The number of nonzero values.
            Defaults to n^(3/2)-n.
        as_sparse: If True, an equivalent sparse CSR matrix is returned.
    Returns:
        A ((n,n) ndarray): A (n, n) strictly diagonally dominant matrix."""

    if num_entries is None:
        num_entries = int(n**1.5) - n
    A = sparse.dok_matrix((n,n))
    rows = np.random.choice(n, size=num_entries)
    cols = np.random.choice(n, size=num_entries)
    data = np.random.randint(-4, 4, size=num_entries)
    for i in range(num_entries):
        A[rows[i], cols[i]] = data[i]
    B = A.tocsr() # convert to row format for the next step
    for i in range(n):
        A[i,i] = abs(B[i]).sum() + 1
    return A.tocsr() if as_sparse else A.toarray()


# Problems 1 and 2
def jacobi(A, b, tol=1e-8, maxiter=100, plot = False):
    """Calculate the solution to the system Ax = b via the Jacobi Method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        b ((n ,) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
    """
    m,n = A.shape
    if m !=n:
        raise ValueError("A must be a square matrix")
    if len(b) != n:
        raise ValueError('b must be the same length as A')
    #create DLU matrices
    x0 = np.zeros(n)
    D = np.diag(np.diag(A))
    # L = np.zeros((n,n))
    # U = np.zeros((n,n))
    # for i in range(n):
    #     D[i,i] = A[i,i]
    # for i in range(1,n):
    #     for j in range(i):
    #         L[i,j] = A[i,j]
    # for j in range(1,n):
    #     for i in range(j):
    #         U[i,j] = A[i,j]
    Dinv = D
    for i in range(n):
        Dinv[i,i] = 1/D[i,i]
    
    
    abserr = []
    for _ in range(maxiter):
        xnext = x0.copy() + Dinv@(b-A@x0)
        abserr.append(la.norm(A@xnext-b, ord = np.inf))
        if la.norm(xnext-x0, ord = np.inf) < tol:
            break
        x0 = xnext.copy()
    
    
    if plot == True:
        plt.semilogy(abserr)
        plt.xlabel('Iteration')
        plt.ylabel('Absolute error of Approximation')
        plt.title('Convergence of Jacobi Method')
    return xnext
        
        # print('A','\n',A,'\n','D','\n',D,'\n','L','\n',L,'\n','U','\n',U)
    


# Problem 3
def gauss_seidel(A, b, tol=1e-8, maxiter=100, plot=False):
    """Calculate the solution to the system Ax = b via the Gauss-Seidel Method.

    Parameters:
        A ((n, n) ndarray): A square matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.
        plot (bool): If true, plot the convergence rate of the algorithm.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    m,n = A.shape
    if m != n:
        raise ValueError("A must be a square matrix")
    if len(b) != n:
        raise ValueError('b must be the same length as A')
        
    x0 = np.zeros(n)
    abserr = []
    for _ in range(maxiter):
        xnext = x0.copy()
        for i in range(n):
            xnext[i] = xnext[i] + (b[i] - (A[i] @ xnext))/A[i,i]
        abserr.append(la.norm((A@xnext)-b, ord = np.inf))
        if la.norm(xnext-x0, ord = np.inf) < tol:
            break
        x0 = xnext.copy()

    if plot == True:
        plt.semilogy(abserr)
        plt.xlabel('Iteration')
        plt.ylabel('Absolute error of Approximation')
        plt.title('Convergence of Gauss-Seidel Method')
        
    return xnext

# Problem 4
def gauss_seidel_sparse(A, b, tol=1e-8, maxiter=100):
    """Calculate the solution to the sparse system Ax = b via the Gauss-Seidel
    Method.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse CSR matrix.
        b ((n, ) ndarray): A vector of length n.
        tol (float): The convergence tolerance.
        maxiter (int): the maximum number of iterations to perform.

    Returns:
        x ((n,) ndarray): The solution to system Ax = b.
    """
    m,n = A.shape
    if m !=n:
        raise ValueError("A must be a square matrix")
    if len(b) != n:
        raise ValueError('b must be the same length as A')
        
    x0 = np.zeros(n)
    for _ in range(maxiter):
        xnext = x0.copy()
        for i in range(n):
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            Aix = A.data[rowstart:rowend] @ x0[A.indices[rowstart:rowend]]
            xnext[i] = xnext[i] + (b[i]-Aix)/A[i,i]
        if la.norm(xnext-x0, ord = np.inf) < tol:
            break
        x0 = xnext.copy()
    return xnext
    

# Problem 5
def sor(A, b, omega, tol=1e-8, maxiter=100):
    """Calculate the solution to the system Ax = b via Successive Over-
    Relaxation.

    Parameters:
        A ((n, n) csr_matrix): A (n, n) sparse matrix.
        b ((n, ) Numpy Array): A vector of length n.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The convergence tolerance.
        maxiter (int): The maximum number of iterations to perform.

    Returns:
        ((n,) ndarray): The solution to system Ax = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of iterations computed.
    """
    m,n = A.shape
    converged = False
    if m !=n:
        raise ValueError("A must be a square matrix")
    if len(b) != n:
        raise ValueError('b must be the same length as A')
        
    x0 = np.zeros(n)
    iters = 0
    for _ in range(maxiter):
        iters+=1
        xnext = x0.copy()
        for i in range(n):
            rowstart = A.indptr[i]
            rowend = A.indptr[i+1]
            Aix = A.data[rowstart:rowend] @ xnext[A.indices[rowstart:rowend]]
            xnext[i] = xnext[i] + omega*(b[i]-Aix)/A[i,i]
        if la.norm(xnext-x0, ord = np.inf) < tol:
            converged = True
            break
        x0 = xnext.copy()
    return xnext, converged, iters


def Agen(n):
    offset = [-1,0,1]
    diagonals = [1,-4,1]
    
    B = sparse.diags(diagonals, offset, shape = (n,n))
    A = sparse.block_diag(tuple([B for k in range(n)]))
    
    A.setdiag(1,-n)
    A.setdiag(1,n)
    
    return A.tocsr()

# Problem 6
def hot_plate(n, omega, tol=1e-8, maxiter=100, plot=False):
    """Generate the system Au = b and then solve it using sor().
    If show is True, visualize the solution with a heatmap.

    Parameters:
        n (int): Determines the size of A and b.
            A is (n^2, n^2) and b is one-dimensional with n^2 entries.
        omega (float in [0,1]): The relaxation factor.
        tol (float): The iteration tolerance.
        maxiter (int): The maximum number of iterations.
        plot (bool): Whether or not to visualize the solution.

    Returns:
        ((n^2,) ndarray): The 1-D solution vector u of the system Au = b.
        (bool): Whether or not Newton's method converged.
        (int): The number of computed iterations in SOR.
    """
    A = Agen(n)
    b = np.zeros(n)
    b[0]=-100
    b[-1]=-100
    bsmall = b.copy()
    for i in range(n-1):
        b = np.concatenate((b,bsmall))
    
    solution, converged, iterations = sor(A,b,omega, tol, maxiter)
    solutionplt = solution.reshape(n,n)
    if plot == True:
        plt.pcolormesh(solutionplt, cmap = 'coolwarm')
        plt.title('Hot Plate')
    
    return solution, converged, iterations
    
# Problem 7
def prob7():
    """Run hot_plate() with omega = 1, 1.05, 1.1, ..., 1.9, 1.95, tol=1e-2,
    and maxiter = 1000 with A and b generated with n=20. Plot the iterations
    computed as a function of omega.
    """
    convergence = []
    for omega in np.arange(1,2,.05):
        convergence.append(hot_plate(20, omega, tol = 1e-2, maxiter = 1000)[2])
    plt.plot(np.arange(1,2,.05),convergence)
    plt.title('Omega Convergence')
    plt.xlabel('Omega value')
    plt.ylabel('Number of Iterations')
    return np.arange(1,2,.05)[np.argmin(convergence)]