# profiling.py
"""Python Essentials: Profiling.
<Name>
<Class>
<Date>
"""

# Note: for problems 1-4, you need only implement the second function listed.
# For example, you need to write max_path_fast(), but keep max_path() unchanged
# so you can do a before-and-after comparison.

import numpy as np
import math
from numba import jit
import time
from matplotlib import pyplot as plt


# Problem 1
def max_path(filename="triangle.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
    def path_sum(r, c, total):
        """Recursively compute the max sum of the path starting in row r
        and column c, given the current total.
        """
        total += data[r][c]
        if r == len(data) - 1:          # Base case.
            return total
        else:                           # Recursive case.
            return max(path_sum(r+1, c,   total),   # Next row, same column
                       path_sum(r+1, c+1, total))   # Next row, next column

    return path_sum(0, 0, 0)            # Start the recursion from the top.

def max_path_fast(filename="triangle_large.txt"):
    """Find the maximum vertical path in a triangle of values."""
    with open(filename, 'r') as infile:
        data = [[int(n) for n in line.split()]
                        for line in infile.readlines()]
        
    maxsum = 0
    path = []
    for i in range(1,len(data)+1):
        maxval = max(data[-i])
        maxsum += maxval     
        path.insert(0,maxval)
    return list(path)


# Problem 2
def primes(N):
    """Compute the first N primes."""
    primes_list = []
    current = 2
    while len(primes_list) < N:
        isprime = True
        for i in range(2, current):     # Check for nontrivial divisors.
            if current % i == 0:
                isprime = False
        if isprime:
            primes_list.append(current)
        current += 1
    return primes_list


def primes_fast(N):
    primes_list = [2]
    current = 3
    while len(primes_list) < N:
        isprime = True
        cursqr = int(math.sqrt(current))+1
        for i in range(3, cursqr, 2):     # Check for nontrivial divisors.
            if current % i == 0:
                isprime = False
                break
        if isprime:
            primes_list.append(current)
        current += 2
    return primes_list

# Problem 3
def nearest_column(A, x):
    """Find the index of the column of A that is closest to x.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    distances = []
    for j in range(A.shape[1]):
        distances.append(np.linalg.norm(A[:,j] - x))
    return np.argmin(distances)

def nearest_column_fast(A, x):
    """Find the index of the column of A that is closest in norm to x.
    Refrain from using any loops or list comprehensions.

    Parameters:
        A ((m,n) ndarray)
        x ((m, ) ndarray)

    Returns:
        (int): The index of the column of A that is closest in norm to x.
    """
    return np.argmin(np.linalg.norm(A.T - x, axis = 1))


# Problem 4
def name_scores(filename="names.txt"):
    """Find the total of the name scores in the given file."""
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    for i in range(len(names)):
        name_value = 0
        for j in range(len(names[i])):
            alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            for k in range(len(alphabet)):
                if names[i][j] == alphabet[k]:
                    letter_value = k + 1
            name_value += letter_value
        total += (names.index(names[i]) + 1) * name_value
    return total

def name_scores_fast(filename='names.txt'):
    """Find the total of the name scores in the given file."""
    
    alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphabet = {}
    i = 1
    for a in alph:
        alphabet[a] = i
        i += 1
        
    with open(filename, 'r') as infile:
        names = sorted(infile.read().replace('"', '').split(','))
    total = 0
    for n in range(len(names)):
        name_value = sum([alphabet[letter] for letter in names[n]])
        total += (name_value*(n+1))
    return total


# Problem 5
def fibonacci():
    """Yield the terms of the Fibonacci sequence with F_1 = F_2 = 1."""
    prev = 1
    nxt = 1
    i = 1
    while True:
        if i < 3:
            yield(1)
        else:  
            current = prev + nxt
            prev = nxt
            nxt = current

            yield current
        i += 1
    
    

def fibonacci_digits(N=1000):
    """Return the index of the first term in the Fibonacci sequence with
    N digits.

    Returns:
        (int): The index.
    """
    for i, x in enumerate(fibonacci()):
        if len(str(x)) >= N:
            print(i+1)
            break


# Problem 6
def prime_sieve(N):
    """Yield all primes that are less than N."""
    ints = np.arange(2, N)
    while True:
        first = ints[0]
        mask = tuple([(ints % first) != 0])
        ints = ints[mask]
        yield first
        if len(ints) == 0:
            break


# Problem 7
def matrix_power(A, n):
    """Compute A^n, the n-th power of the matrix A."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product


@jit
def matrix_power_numba(A, n):
    """Compute A^n, the n-th power of the matrix A, with Numba optimization."""
    product = A.copy()
    temporary_array = np.empty_like(A[0])
    m = A.shape[0]
    for power in range(1, n):
        for i in range(m):
            for j in range(m):
                total = 0
                for k in range(m):
                    total += product[i,k] * A[k,j]
                temporary_array[j] = total
            product[i] = temporary_array
    return product

def prob7(n=10):
    """Time matrix_power(), matrix_power_numba(), and np.linalg.matrix_power()
    on square matrices of increasing size. Plot the times versus the size.
    """
    M = [2**n for n in range(2,8)]
    matpow = []
    matpownum = []
    linmatpow = []
    
    A = np.random.random((1,1))
    matrix_power_numba(A, n)

    for m in M:
        A = np.random.random((m,m))
        
        startmatpow = time.perf_counter()
        matrix_power(A, n)
        matpow.append(time.perf_counter() - startmatpow)
        
        startmatpownum = time.perf_counter()
        matrix_power_numba(A, n)
        matpownum.append(time.perf_counter() - startmatpownum)
        
        startlinmatpow = time.perf_counter()
        np.linalg.matrix_power(A,n)
        linmatpow.append(time.perf_counter() - startlinmatpow)
     
    ax1 = plt.subplot(121)
    ax1.plot(M, matpow, color = 'k', label = 'matrix_power time')
    ax1.plot(M, matpownum, color = 'blue', label = 'matrix_power_numba time')
    ax1.plot(M, linmatpow, color = 'red', label = 'linalg.matrix_power time')
    plt.title('Linear Time')
    plt.xlabel('Matrix Size')
    plt.ylabel('Time in Seconds')
    plt.legend(loc = 'upper left')
    
    ax2 = plt.subplot(122)
    ax2.loglog(M, matpow, color = 'k', label = 'matrix_power time')
    ax2.loglog(M, matpownum, color = 'blue', label = 'matrix_power_numba time')
    ax2.loglog(M, linmatpow, color = 'red', label = 'linalg.matrix_power time')
    plt.title('Log Time')
    
    plt.tight_layout()
    plt.show()
    
    
    
    
    
    
    