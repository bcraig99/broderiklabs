# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
<Name>
<Class>
<Date>
"""

# (Optional) Import functions from your QR Decomposition lab.
# import sys
# sys.path.insert(1, "../QR_Decomposition")
# from qr_decomposition import qr_gram_schmidt, qr_householder, hessenberg

import numpy as np
from matplotlib import pyplot as plt
from scipy import linalg as la
import cmath

# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    Q , R = la.qr(A,mode='economic')
    return la.solve_triangular(R,Q.T @ b)
    
    
# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    housing = np.load("housing.npy")   #Import housing data
    ones = np.ones((len(housing),1))   #Create 1s column vector
    x = [housing[h][0] for h in range(len(housing))] #extract x values
    x = np.array(x).reshape(len(x),1)  
    b = [housing[h][1] for h in range(len(housing))] #xtract y values
    A = np.hstack((x,ones))            #Stack xs with ones
    slope, intercept = least_squares(A,b) #slope and intercept are the values from least_squares
    point1 = [0, intercept]         #two points to draw between
    point2 = [len(housing)/2,slope*len(housing)/2+intercept]
    x_values = [point1[0], point2[0]]
    y_values = [point1[1], point2[1]]
    ax=plt.subplot(1,1,1)  #Plot it
    ax.scatter(x,b,c='k',label="Actual Mean Price")
    ax.plot(x_values, y_values, color = 'k', label="Least Squares Fit")
    ax.set_xlabel("Year from 2000-2016")
    ax.set_ylabel("Price in thousands of dollars")
    ax.legend()
    plt.title('Housing Prices')
    plt.show()

# Problem 3
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """
    housing = np.load("housing.npy")
    m=len(housing)
    n=3
    solutions=[]
    for n in [3,6,9,12]:
        Ax=[housing[x][0] for x in range(m)]  #xvalues
        b=[housing[y][1] for y in range(m)]   #yvalues
        A=np.vander(Ax,n+1)                   #Vander matrix
        solutions.append(la.lstsq(A,b)[0])
    
    
    #Plotting stuff
    plt.suptitle("Polynomial fit fuctions on Mean Housing Price")
    
    x=np.linspace(0,m/2-1)
    ax1=plt.subplot(2,2,1)    
    f3 = np.poly1d(solutions[0])
    ax1.plot(x,f3(x),color='b',linewidth=1,label="Degree-3")
    ax1.scatter(Ax,b,marker='*',color='k',s=2,label='Housing Data')
    plt.title('Degree-3',size = 10)
    ax1.set_xlabel("Year from 2000-2016",size=8)
    ax1.set_ylabel("Price in thousands of dollars",size=8)
    
    f6 = np.poly1d(solutions[1])
    ax2=plt.subplot(2,2,2)
    ax2.plot(x,f6(x),color='r',linewidth=1,label="Degree-6")
    ax2.scatter(Ax,b,marker='*',color='k',s=2)
    plt.title('Degree-6',size = 10)
    
    ax3=plt.subplot(2,2,3)
    f9 = np.poly1d(solutions[1])
    ax3.plot(x,f9(x),color='c',linewidth=1,label="Degree-9")
    ax3.scatter(Ax,b,marker='*',color='k',s=2)
    plt.title('Degree-9',size = 10)
    
    ax4=plt.subplot(2,2,4)
    f12 = np.poly1d(solutions[1])
    ax4.plot(x,f12(x),color='g',linewidth=1,label="Polynomial Fit Function")
    ax4.scatter(Ax,b,marker='*',color='k',s=2,label='Housing Price')
    plt.title('Degree-12',size = 10)
    plt.legend(prop={'size': 7})

    plt.subplots_adjust(wspace=.2, hspace=.7)
    plt.show()

    #x = la.lstsq(A, b)[0]

def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t)
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    ellipse = np.load('ellipse.npy')
    m=len(ellipse)
    A = [[ellipse[i][0]**2, ellipse[i][0], ellipse[i][1]*ellipse[i][0], ellipse[i][1], ellipse[i][1]**2] for i in range(m)] #Coefficients for the ellipse function
    b = np.ones((m,1))
    c=la.lstsq(A,b)[0]
        
    plt.plot([ellipse[i][0] for i in range(m)],[ellipse[i][1] for i in range(m)],'k*')
    plot_ellipse(c[0],c[1],c[2],c[3],c[4])
    plt.show


# Problem 5
def power_method(A, N=20, tol=1e-12):
    """Compute the dominant eigenvalue of A and a corresponding eigenvector
    via the power method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The maximum number of iterations.
        tol (float): The stopping tolerance.

    Returns:
        (float): The dominant eigenvalue of A.
        ((n,) ndarray): An eigenvector corresponding to the dominant
            eigenvalue of A.
    """
    m,n = np.shape(A)
    x=np.random.rand(n,1) #Random column vector
    x = x/la.norm(x)      #normalized
    for k in range(N):
        x2= np.copy(x)    #make a copy
        x = A @ x         #redefine as the vector x A
        x = x/la.norm(x)  #normalize
        if la.norm(x-x2) < tol: #If we're within tolerance away from the previous
            break
        
    return x.T @ (A @ x) , x


# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
    """Compute the eigenvalues of A via the QR algorithm.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The number of iterations to run the QR algorithm.
        tol (float): The threshold value for determining if a diagonal S_i
            block is 1x1 or 2x2.

    Returns:
        ((n,) ndarray): The eigenvalues of A.
    """

    m, n = np.shape(A)
    S = la.hessenberg(A)
    for k in range(N):
        Q, R = la.qr(S)
        S = R @ Q
    eigs = []
    i = 0
    while i<n:
        # print(i)
        # Q, R = la.qr(S)
        # S = R @ Q
        
        if i == n-1:
            eigs.append(S[i][i])
        
        elif abs(S[i+1][i]) < tol:
            eigs.append(S[i][i])
        
        else:
            mean = (S[i][i] + S[i+1][i+1]) / 2
            det = (S[i][i] * S[i+1][i+1]) - (S[i][i+1] * S[i+1][i])
            
            eigs.append(mean + cmath.sqrt(mean**2 - det))
            eigs.append(mean - cmath.sqrt(mean**2 - det))
            i+=1
        i+=1
   
    return eigs