# condition_stability.py
"""Volume 1: Conditioning and Stability.
<Name>
<Class>
<Date>
"""

import numpy as np
import sympy as sy
import scipy.linalg as la
from matplotlib import pyplot as plt

# Problem 1
def matrix_cond(A):
    """Calculate the condition number of A with respect to the 2-norm."""
    singvals = la.svd(A)[1]
    if min(singvals) == 0:
        return np.inf
    kappa = max(singvals)/min(singvals)
    return kappa

# Problem 2
def prob2():
    """Randomly perturb the coefficients of the Wilkinson polynomial by
    replacing each coefficient c_i with c_i*r_i, where r_i is drawn from a
    normal distribution centered at 1 with standard deviation 1e-10.
    Plot the roots of 100 such experiments in a single figure, along with the
    roots of the unperturbed polynomial w(x).

    Returns:
        (float) The average absolute condition number.
        (float) The average relative condition number.
    """
    w_roots = np.arange(1, 21)

    # Get the exact Wilkinson polynomial coefficients using SymPy.
    x, i = sy.symbols('x i')
    w = sy.poly_from_expr(sy.product(x-i, (i, 1, 20)))[0]
    w_coeffs = np.array(w.all_coeffs())
    kaphat = []
    cond = []
    new_roots = np.array([])
    for _ in range(100):
        r = np.random.normal(1,1e-10, size = len(w_coeffs))
        new_coeffs = w_coeffs * r
        ruts = np.array(np.sort(np.roots(np.poly1d(new_coeffs))))
        new_roots = np.append(new_roots, ruts)
    
        kappahat = la.norm(ruts - w_roots, ord = np.inf)/la.norm(r, ord = np.inf)
        kappa =  kappahat * la.norm(w_coeffs, ord = np.inf) / la.norm(w_roots, ord = np.inf)
        kaphat.append(kappahat)
        cond.append(kappa)
    
    plt.scatter(np.real(w_roots),np.imag(w_roots), label = 'Original Roots')
    plt.scatter(np.real(new_roots), np.imag(new_roots), s = .5, color = 'k', label = 'Perturbed Roots')
    plt.legend()
    plt.show()
    return np.mean(kaphat), np.mean(cond)

# Helper function
def reorder_eigvals(orig_eigvals, pert_eigvals):
    """Reorder the perturbed eigenvalues to be as close to the original eigenvalues as possible.
    
    Parameters:
        orig_eigvals ((n,) ndarray) - The eigenvalues of the unperturbed matrix A
        pert_eigvals ((n,) ndarray) - The eigenvalues of the perturbed matrix A+H
        
    Returns:
        ((n,) ndarray) - the reordered eigenvalues of the perturbed matrix
    """
    n = len(pert_eigvals)
    sort_order = np.zeros(n).astype(int)
    dists = np.abs(orig_eigvals - pert_eigvals.reshape(-1,1))
    for _ in range(n):
        index = np.unravel_index(np.argmin(dists), dists.shape)
        sort_order[index[0]] = index[1]
        dists[index[0],:] = np.inf
        dists[:,index[1]] = np.inf
    return pert_eigvals[sort_order]

# Problem 3
def eig_cond(A):
    """Approximate the condition numbers of the eigenvalue problem at A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) The absolute condition number of the eigenvalue problem at A.
        (float) The relative condition number of the eigenvalue problem at A.
    """
    H = np.random.normal(0, 1e-10, size = A.shape)
    Atil = A + H
    eigvals = la.eig(A)[0]
    eigvalstil = la.eig(Atil)[0]
    eigvalstil = reorder_eigvals(eigvals, eigvalstil)
    kappahat = la.norm(eigvals - eigvalstil, np.inf)/la.norm(H, 2)
    kappa = la.norm(A, 2)*kappahat/la.norm(eigvals)
    return kappahat, kappa

# Problem 4
def prob4(domain=[-100, 100, -100, 100], res=50):
    """Create a grid [x_min, x_max] x [y_min, y_max] with the given resolution. For each
    entry (x,y) in the grid, find the relative condition number of the
    eigenvalue problem, using the matrix   [[1, x], [y, 1]]  as the input.
    Use plt.pcolormesh() to plot the condition number over the entire grid.

    Parameters:
        domain ([x_min, x_max, y_min, y_max]):
        res (int): number of points along each edge of the grid.
    """
    stuff = []
    xrange = np.linspace(domain[0],domain[1],res)
    yrange = np.linspace(domain[2],domain[3],res)
    for x in xrange:
        for y in yrange:
            A = np.array([1,x,y,1]).reshape(2,2)
            stuff.append(eig_cond(A)[1])
    stuff = np.array(stuff).reshape((res,res))
    plt.pcolormesh(xrange, yrange, stuff, cmap = 'gray_r')

# Problem 5
def prob5(n):
    """Approximate the data from "stability_data.npy" on the interval [0,1]
    with a least squares polynomial of degree n. Solve the least squares
    problem using the normal equation and the QR decomposition, then compare
    the two solutions by plotting them together with the data. Return
    the mean squared error of both solutions, ||Ax-b||_2.

    Parameters:
        n (int): The degree of the polynomial to be used in the approximation.

    Returns:
        (float): The forward error using the normal equations.
        (float): The forward error using the QR decomposition.
    """
    xk, b = np.load('stability_data.npy').T
    A = np.vander(xk, n+1)
    inverse_version = la.inv(A.T@A) @ (A.T) @ b.T
    inverse_error = la.norm((A@inverse_version) - b, ord = 2)
    
    Q, R = la.qr(A,mode = 'economic')
    qr_version = la.solve_triangular(R, Q.T@b)
    qr_error = la.norm((A@qr_version) - b, 2)
    
    invpoly = np.polyval(inverse_version, xk)
    qrpoly  = np.polyval(qr_version, xk)
    
    plt.plot(xk, invpoly, label = 'Inverse Formula')
    plt.plot(xk, qrpoly, label = 'QR Decomp Formula', alpha = .75)
    plt.scatter(xk, b, label = 'Original Datapoints', color = 'k', s = 10, alpha = .8)
    plt.title('Stability at n = '+str(n))
    plt.legend()    
    plt.show()
    
    return inverse_error, qr_error
# Problem 6
def prob6():
    """For n = 5, 10, ..., 50, compute the integral I(n) using SymPy (the
    true values) and the subfactorial formula (may or may not be correct).
    Plot the relative forward error of the subfactorial formula for each
    value of n. Use a log scale for the y-axis.
    """
    
    error = []
    x = sy.symbols('x')
    N = np.arange(5,55,5)
    for n in N:
        I = (x**int(n))*sy.exp(x-1)
        syver = sy.integrate(I, (x,int(0),int(1)))
        sumver = ((int(-1))**int(n))*(sy.subfactorial(int(n))-(sy.factorial(int(n))/np.e))
        error.append(float(np.real(abs(syver - sumver)/abs(syver))))
    
    plt.yscale('log')
    plt.plot(N, error, marker = 'x')
    plt.title('Relative forward error')