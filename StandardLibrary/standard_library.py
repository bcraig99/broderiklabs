# standard_library.py
"""Python Essentials: The Standard Library.
<Name>
<Class>
<Date>
"""
import calculator as calc
# import itertools
from itertools import combinations, chain
import box
import random
import time
from sys import argv

# Problem 1
def prob1(L):
    """Return the minimum, maximum, and average of the entries of L
    (in that order).
    """
    mini=min(L)
    maxi=max(L)
    avg=sum(L)/len(L)
    
    return mini, maxi, avg
    
    
# Problem 2
def prob2():
    """Determine which Python objects are mutable and which are immutable.
    Test numbers, strings, lists, tuples, and sets. Print your results.
    """
    integer=2
    integer2=integer
    integer2+=2
    if integer==integer2:
        print('integer type IS mutable')
    else:
        print('integer type is IMMUTABLE')
    
    string=str('fish')
    string2=string
    string2='fisher of '+string2
    if string==string2:
        print('string type IS mutable')
    else:
        print('string type is IMMUTABLE')
    
    list1=[1,2,3,4,5,6]
    list2=list1
    list2[1]='a'
    if list1==list2:
        print('list type IS mutable')
    else:
        print('list type is IMMUTABLE')
    
    my_tuple=(1,1)
    my_tuple_copy=my_tuple
    my_tuple_copy+=(1,)
    if my_tuple==my_tuple_copy:
        print('tuple type IS mutable')
    else:
        print('tuple type is IMMUTABLE')

    MySet={1,2,3,4,5,6,7,8,9,0}
    MySetCopy=MySet
    MySetCopy.add('d')
    if MySet==MySetCopy:
        print('set type IS mutable')
    else:
        print('set type is IMMUTABLE')


# Problem 3
def hypot(a, b):
    asquare=calc.product(a,a)
    bsquare=calc.product(b,b)
    aplusbsquare=calc.sum(asquare,bsquare)
    sqrtcsquare=calc.sqrt(aplusbsquare)
    return sqrtcsquare

# Problem 4
def power_set(A):
    
    lst=list(A)     #Cast A as a list
    powerset=[]     #Create empty list
    for i in range(len(A)+1):   #Iterate as many times as there are elements of A
        k=chain(set(combinations(lst,i)))       #k is a list of combinations of i elements of A
        k=list(k)   #recast k as a list
        for r in range(len(k)):     #Iterate through the combinations of i elements
            powerset.append(k[r])   #Add each element of those combinations
                
    for k in range(len(powerset)):  #change each element to a set
        powerset[k]=set(powerset[k])   
    
    return powerset    #Return a list because sets are immutable lists of mutable objects

# Problem 5: Implement shut the box.
def shut_the_box(player, timelimit):
    start=time.time()            #Set start point
    # if len(sys.argv) == 3:
    #     return "To play, please input your name and time limit."
    finished=False
    NumbersLeft=[0,1,2,3,4,5,6,7,8,9]   #I included 0 so only one number can be closed
    while finished == False: 
        now=time.time()
        if now-start>timelimit:  #Timer
            print("Time's up!")
            finished=True
            continue
        public=NumbersLeft.copy()  #This is to show to the player what numbers they have left
        public.remove(0)
        print("These are the numbers left", public)
        print('You have', round(timelimit-(time.time()-start)), 'seconds remaining')
        print("Let's roll!")
        if sum(NumbersLeft)<=6:
            roll = random.randint(1,6)
        else:
            roll = random.randint(2,12)
        print('You rolled a' , roll)
        if box.isvalid(roll,NumbersLeft)==False:
            print("No valid moves left. You lose.")
            finished=True
            continue
        
        play=False
        while play == False:
            player_input = input("Which numbers do you want to close? Enter your input as two integers separated by a space.")
            if player_input == 'help':
                print('These are the numbers remaining', NumbersLeft)
                continue
            move = box.parse_input(player_input,NumbersLeft)
            if move == [] :
                print("Input invalid.")
                print('You have', round(timelimit-(time.time()-start)), 'seconds remaining')
                continue
            elif sum(move) != roll:
                print('You have', round(timelimit-(time.time()-start)), 'seconds remaining')
                print("Input invalid.")
                continue
            else:
                if move[0]!=0:
                    NumbersLeft.remove(move[0])
                if move[1]!=0:
                    NumbersLeft.remove(move[1])
                play=True
        
        if len(NumbersLeft) == 1:
            return "You win!"
        else:
            continue
       
    return "Thanks for playing!"

if __name__ == "__main__":
    shut_the_box(argv[1],int(argv[2]))