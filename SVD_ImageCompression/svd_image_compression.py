# solutions.py
"""Volume 1: The SVD and Image Compression. Solutions File."""

from scipy import linalg as la
import numpy as np
from matplotlib import pyplot as plt
import numpy.linalg as nla
from imageio import imread


# Problem 1
def compact_svd(A, tol=1e-6):
    """Compute the truncated SVD of A.

    Parameters:
        A ((m,n) ndarray): The matrix (of rank r) to factor.
        tol (float): The tolerance for excluding singular values.

    Returns:
        ((m,r) ndarray): The orthonormal matrix U in the SVD.
        ((r,) ndarray): The singular values of A as a 1-D array.
        ((r,n) ndarray): The orthonormal matrix V^H in the SVD.
    """
    (m, n) = np.shape(A)
    lam, V1 = la.eig(A.conj().T@A)
    sig = np.sqrt(lam)
    sort_index = np.argsort(sig)[::-1]
    sig = np.sort(sig)[::-1]

    r=0
    V=V1[:,sort_index]
    for i in sig:
        if i>tol:
            r+=1
    sig1 = sig[:r]
    V1 = V[:,:r]
    U1 = np.zeros((m,n))

    U1 =(A@V1)/sig1

    return U1, sig1, V1.conj().T

# Problem 2
def visualize_svd(A):
    """Plot the effect of the SVD of A as a sequence of linear transformations
    on the unit circle and the two standard basis vectors.
    """
    theta = np.linspace(0,2*np.pi,200)
    x = np.cos(theta)
    y = np.sin(theta)
    S = np.vstack((np.array(x),np.array(y)))
    E = np.array([1,0,0,0,0,1]).reshape(2,3)
    U, sig, V = la.svd(A)
    
    ax1 = plt.subplot(221)
    ax1.plot(S[0],S[1])
    ax1.plot(E[0],E[1])
    ax1.axis('equal')
    
    ax2 = plt.subplot(222)
    ax2.plot((V@S)[0],(V@S)[1])
    ax2.plot((V@E)[0],(V@E)[1])
    ax2.axis('equal')

    ax3 = plt.subplot(223)
    ax3.plot((np.diag(sig)@V@S)[0],(np.diag(sig)@V@S)[1])
    ax3.plot((np.diag(sig)@V@E)[0],(np.diag(sig)@V@E)[1])
    ax3.axis('equal')
    
    ax4 = plt.subplot(224)
    ax4.plot((U@np.diag(sig)@V@S)[0],(U@np.diag(sig)@V@S)[1])
    ax4.plot((U@np.diag(sig)@V@E)[0],(U@np.diag(sig)@V@E)[1])
    ax4.axis('equal')
    
    plt.axis('equal')
    
    plt.show()
    
    
# Problem 3
def svd_approx(A, s):
    """Return the best rank s approximation to A with respect to the 2-norm
    and the Frobenius norm, along with the number of bytes needed to store
    the approximation via the truncated SVD.

    Parameters:
        A ((m,n), ndarray)
        s (int): The rank of the desired approximation.

    Returns:
        ((m,n), ndarray) The best rank s approximation of A.
        (int) The number of entries needed to store the truncated SVD.
    """
    U, sig, V = la.svd(A)   
    rank = nla.matrix_rank(A)
    if s > rank:
        raise ValueError(str(s)+" is greater than the rank of A: "+str(rank))
    A_s = U[:,:s]@np.diag(sig[:s])@V[:s,:]
    Uhat = U[:,:s]
    sighat = sig[:s]
    Vhat = V[:s,:]
    A_s = Uhat@np.diag(sighat)@Vhat
    return A_s, Uhat.size+Vhat.size+sighat.size

# Problem 4
def lowest_rank_approx(A, err):
    """Return the lowest rank approximation of A with error less than 'err'
    with respect to the matrix 2-norm, along with the number of bytes needed
    to store the approximation via the truncated SVD.

    Parameters:
        A ((m, n) ndarray)
        err (float): Desired maximum error.

    Returns:
        A_s ((m,n) ndarray) The lowest rank approximation of A satisfying
            ||A - A_s||_2 < err.
        (int) The number of entries needed to store the truncated SVD.
    """
    U, sig, V = la.svd(A)
    if err <= sig[-1]:
        raise ValueError("Matrix cannot be approximated within the tolerance by a matrix of lesser rank")
    s=np.argmax(np.where(sig<err, sig, -1))
    return svd_approx(A, s)

# Problem 5
def compress_image(filename, s):
    """Plot the original image found at 'filename' and the rank s approximation
    of the image found at 'filename.' State in the figure title the difference
    in the number of entries used to store the original image and the
    approximation.

    Parameters:
        filename (str): Image file path.
        s (int): Rank of new image.
    """
    image = imread(filename)
    image = image/255
    if len(image.shape)==3:
        red_layer = image[:,:,0]
        blue_layer = image[:,:,1]
        green_layer = image[:,:,2]
        
        red = svd_approx(red_layer, s)
        blue = svd_approx(blue_layer, s)
        green = svd_approx(green_layer, s)
        
        compsize = red[1]+blue[1]+green[1]
        
        image_compressed = np.dstack((red[0],blue[0],green[0]))
        
        ax1 = plt.subplot(121)
        ax1.imshow(np.clip(image_compressed,0,1))
        ax1.axis('off')
        plt.title('Compressed')
        
        ax2 = plt.subplot(122)
        ax2.imshow(image)
        plt.title('Original')
        plt.axis('off')
        
        plt.suptitle("Image compression s = " +str(s)+", size difference =" +str(image.size-compsize))
        plt.show()
    
    else:
        image_compressed = svd_approx(image, s)
        compsize = image_compressed[1]
        ax1 = plt.subplot(121)
        ax1.imshow(np.clip(image_compressed[0],0,1), cmap='gray')
        ax1.axis('off')
        plt.title('Compressed')
        
        ax2 = plt.subplot(122)
        ax2.imshow(image, cmap='gray')
        plt.title('Original')
        plt.axis('off')
        
        plt.suptitle("Image compression s = " +str(s)+", size difference =" +str(image.size-compsize))
        plt.show()

    

if __name__ == '__main__':
#     A = np.random.random((5,5))
    # visualize_svd(A)
    # print(svd_approx(A, 2))
    
    # print(lowest_rank_approx(A, .2))
    # U, sig, V = la.svd(A)
    # print(sig)
    # print(la.norm(lowest_rank_approx(A, .5)[0]-A))
    # print(imread('C:/Users/magic/ACME/Profile.JPG').shape)
    # print(compress_image('C:/Users/magic/ACME/Profile.JPG',25))
    print(compress_image('hubble.jpg',25))