# qr_decomposition.py
"""Volume 1: The QR Decomposition.
<Name>
<Class>
<Date>
"""
import numpy as np
from scipy import linalg as lin


# Problem 1
def qr_gram_schmidt(A):
    """Compute the reduced QR decomposition of A via Modified Gram-Schmidt.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,n) ndarray): An orthonormal matrix.
        R ((n,n) ndarray): An upper triangular matrix.
    """
    m,n = np.shape(A)   #Store the dimensions of A.
    Q = A.copy()        #Make a copy of A with np.copy()
    R = np.zeros((n,n)) #An n × n array of all zeros.
    for i in range(n):
        R[i,i] = lin.norm(Q[:,i])
        Q[:,i] = Q[:,i] / R[i,i] #Normalize the ith column of Q.
        for j in range(i+1,n):
            R[i,j] = Q[:,j].T @ Q[:,i]
            Q[:,j] = Q[:,j] - (R[i,j] * Q[:,i])  #Orthogonalize the jth column of Q
    return -Q, -R


# Problem 2
def abs_det(A):
    """Use the QR decomposition to efficiently compute the absolute value of
    the determinant of A.

    Parameters:
        A ((n,n) ndarray): A square matrix.

    Returns:
        (float) the absolute value of the determinant of A.
    """
    
    
    print(abs(np.prod(np.diagonal([lin.qr(A)[1]]))))
    


# Problem 3
def solve(A, b):
    """Use the QR decomposition to efficiently solve the system Ax = b.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.
        b ((n, ) ndarray): A vector of length n.

    Returns:
        x ((n, ) ndarray): The solution to the system Ax = b.
    """
    (Q,R)=lin.qr(A)  #Calculate Q and R
    y=Q.T @ b.T
    n=len(b)
    x=[0]*n
    for j in reversed(range(n)): #back substitute
        sumvalue=y[j]
        for i in reversed(range(j,n)):
            sumvalue-=R[j,i]*x[i]
        x[j]=sumvalue/R[j,j]
    return x
    # return lin.solve(R,y)
    
sign = lambda x: 1 if x >= 0 else -1

# Problem 4
def qr_householder(A):
    """Compute the full QR decomposition of A via Householder reflections.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n.

    Returns:
        Q ((m,m) ndarray): An orthonormal matrix.
        R ((m,n) ndarray): An upper triangular matrix.
    """
    (m,n)=np.shape(A)
    R=np.copy(A)
    Q=np.eye(m)              #The m × m identity matrix.
    for k in range(n):
        u=np.copy(R[k:,k])
        u[0]=u[0]+sign(u[0])*lin.norm(u) #u0 is the first entry of u.
        u=u/lin.norm(u)                  #Normalize u
        R[k:,k:]=R[k:,k:]-np.outer(2*u,(u.T @ R[k:,k:])) #Apply the reflection to R
        Q[k:,:] = Q[k:,:]-np.outer(2*u,(u.T @ Q[k:,:]))  #Apply the reflection to Q
        
    return Q.T, R


# Problem 5
def hessenberg(A):
    """Compute the Hessenberg form H of A, along with the orthonormal matrix Q
    such that A = QHQ^T.

    Parameters:
        A ((n,n) ndarray): An invertible matrix.

    Returns:
        H ((n,n) ndarray): The upper Hessenberg form of A.
        Q ((n,n) ndarray): An orthonormal matrix.
    """
    (m,n)=np.shape(A)
    H=np.copy(A)
    Q=np.eye(m)
    for k in range(n-2):
        u = np.copy(H[k+1:,k])
        u[0] = u[0]+sign(u[0])*lin.norm(u)
        u = u / lin.norm(u)
        H[k+1:,k:] = H[k+1:,k:] - np.outer(2*u,u.T @ H[k+1:,k:])  #Apply Qk to H
        H[:,k+1:] = H[:,k+1:] - 2*np.outer(H[:,k+1:] @ u , u.T)   #Apply QTk to H
        Q[k+1:,:] = Q[k+1:,:] - np.outer(2*u, u.T @ Q[k+1:,:])    #Apply Qk to Q
    
    return H, Q.T


def testmat(n):
    return np.array(list(np.random.rand(n**2))).reshape(n,n)
    
