# image_segmentation.py
"""Volume 1: Image Segmentation.
<Name>
<Class>
<Date>
"""

import numpy as np
from scipy import linalg as la
from imageio import imread
from matplotlib import pyplot as plt
import scipy
import scipy.sparse.linalg as sla

# Problem 1
def laplacian(A):
    """Compute the Laplacian matrix of the graph G that has adjacency matrix A.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.

    Returns:
        L ((N,N) ndarray): The Laplacian matrix of G.
    """
    n = np.shape(A)[0]
    D = np.zeros((n,n))
    for i in range(n):   
        D[i,i]=A[i].sum(axis=0)
    L = D - A
    return L


# Problem 2
def connectivity(A, tol=1e-8):
    """Compute the number of connected components in the graph G and its
    algebraic connectivity, given the adjacency matrix A of G.

    Parameters:
        A ((N,N) ndarray): The adjacency matrix of an undirected graph G.
        tol (float): Eigenvalues that are less than this tolerance are
            considered zero.

    Returns:
        (int): The number of connected components in G.
        (float): the algebraic connectivity of G.
    """
    
    eigenvals = np.real(la.eig(laplacian(A))[0])
    for i in range(len(eigenvals)):
        if eigenvals[i] < tol:
            eigenvals[i]=0
    eigenvals.sort()
    return len(eigenvals[eigenvals==0]), eigenvals[1]

# Helper function for problem 4.
def get_neighbors(index, radius, height, width):
    """Calculate the flattened indices of the pixels that are within the given
    distance of a central pixel, and their distances from the central pixel.

    Parameters:
        index (int): The index of a central pixel in a flattened image array
            with original shape (radius, height).
        radius (float): Radius of the neighborhood around the central pixel.
        height (int): The height of the original image in pixels.
        width (int): The width of the original image in pixels.

    Returns:
        (1-D ndarray): the indices of the pixels that are within the specified
            radius of the central pixel, with respect to the flattened image.
        (1-D ndarray): the euclidean distances from the neighborhood pixels to
            the central pixel.
    """
    # Calculate the original 2-D coordinates of the central pixel.
    row, col = index // width, index % width

    # Get a grid of possible candidates that are close to the central pixel.
    r = int(radius)
    x = np.arange(max(col - r, 0), min(col + r + 1, width))
    y = np.arange(max(row - r, 0), min(row + r + 1, height))
    X, Y = np.meshgrid(x, y)

    # Determine which candidates are within the given radius of the pixel.
    R = np.sqrt(((X - col)**2 + (Y - row)**2))
    mask = R < radius
    return (X[mask] + Y[mask]*width).astype(np.int), R[mask]


# Problems 3-6
class ImageSegmenter:
    """Class for storing and segmenting images."""

    # Problem 3
    def __init__(self, filename):
        """Read the image file. Store its brightness values as a flat array."""
        
        self.image = imread(filename) # Read a (very) small image.
        self.scaled = self.image / 255 # Scale the image to floats between 0 and 1 for Matplotlib
        self.m=self.image.shape[0]
        self.n=self.image.shape[1]
        if len(self.image.shape)==3:  
            self.brightness = np.ravel(self.scaled.mean(axis=2)) # Average over the last axis.

        else:
            self.brightness = np.ravel(self.scaled)
        
    # Problem 3
    def show_original(self):
        """Display the original image."""
        
        if len(self.scaled.shape)==2:
            plt.imshow(self.scaled, cmap='gray')
            plt.axis('off')
        else:
            plt.imshow(self.scaled)
            plt.axis('off')
            
    # Problem 4
    def adjacency(self, r=5., sigma_B2=.02, sigma_X2=3.):
        """Compute the Adjacency and Degree matrices for the image graph."""
        A = scipy.sparse.lil_matrix((self.m*self.n,self.m*self.n))
        D = []
        for i in range(self.m*self.n):
            J,dist = get_neighbors(i, r, self.m, self.n)
            weights = [np.exp((-abs(self.brightness[i]-self.brightness[J[j]])/sigma_B2)-dist[j]/sigma_X2) for j in range(len(J))]
            A[i,J]=weights
            D.append(sum(weights))
        A = scipy.sparse.csc_matrix(A)
        return A, np.array(D)
        
    def cut(self, A, D):
        """Compute the boolean mask that segments the image."""
        L = scipy.sparse.csgraph.laplacian(A)
        D = [1/np.sqrt(D[i]) for i in range(len(D))]
        D = np.array(D)
        Dhalf = scipy.sparse.diags(D)
        DLD = Dhalf @ L @ Dhalf
        eig = sla.eigsh(DLD, which='SM',k=2)[1][:,1]
        eig = np.array(eig).reshape(self.m,self.n)   
        mask = eig>0
        return mask
        
    # Problem 6
    def segment(self, r=5., sigma_B=.02, sigma_X=3.):
        """Display the original image and its segments."""
        mask = self.cut(self.adjacency()[0],self.adjacency()[1])

        

        if len(self.scaled.shape)==2:
            positive = np.multiply(mask,self.scaled)
            negative = np.multiply(~mask,self.scaled)
            
            plt.subplot(311)
            plt.imshow(self.scaled, cmap='gray')
            plt.title('Original', fontdict = {'fontsize' : 8})
            plt.axis('off')
            
            plt.subplot(312)
            plt.imshow(positive, cmap='gray')
            plt.title('Positive Mask', fontdict = {'fontsize' : 8})
            plt.axis('off')
            
            plt.subplot(313)
            plt.imshow(negative, cmap='gray')
            plt.title('Negative Mask', fontdict = {'fontsize' : 8})
            plt.axis('off')
            plt.subplots_adjust(wspace=.3, hspace=.5)
            plt.show()
            
            
        else:
            positive = np.multiply(np.dstack((mask,mask,mask)),self.scaled)
            negative = np.multiply(np.dstack((~mask,~mask,~mask)),self.scaled)
            
            plt.subplot(311)
            plt.imshow(self.scaled)
            plt.title('Original', fontdict = {'fontsize' : 8})
            plt.axis('off')
              
            plt.subplot(312)
            plt.imshow(positive)
            plt.title('Positive Mask', fontdict = {'fontsize' : 8})
            plt.axis('off')
   
            plt.subplot(313)
            plt.imshow(negative)
            plt.title('Negative Mask', fontdict = {'fontsize' : 8})
            
            plt.subplots_adjust(wspace=.1, hspace=.5)
            plt.axis('off')
            plt.show()
        

# if __name__ == '__main__':
    # ImageSegmenter("C:/Users/magic/ACME/Profile.JPG").segment()
#     ImageSegmenter("dream.png").segment()
#     ImageSegmenter("monument_gray.png").segment()
#     ImageSegmenter("monument.png").segment()
