# differentiation.py
"""Volume 1: Differentiation.
<Name>
<Class>
<Date>
"""
import sympy as sy
import numpy as np
from matplotlib import pyplot as plt
from autograd import numpy as anp
from autograd import grad
from autograd import elementwise_grad
import random
import time

# Problem 1
def prob1():
    """Return the derivative of (sin(x) + 1)^sin(cos(x)) using SymPy."""
    x = sy.symbols('x')
    f = (sy.sin(x)+1)**(sy.sin(sy.cos(x)))
    return sy.lambdify(x,sy.diff(f, x))


# Problem 2
def fdq1(f, x, h=1e-5):
    """Calculate the first order forward difference quotient of f at x."""
    return((f(x+h)-f(x))/h)


def fdq2(f, x, h=1e-5):
    """Calculate the second order forward difference quotient of f at x."""

    return (-3*f(x)+4*f(x+h)-f(x+2*h))/(2*h)


def bdq1(f, x, h=1e-5):
    """Calculate the first order backward difference quotient of f at x."""

    return (f(x)-f(x-h))/h


def bdq2(f, x, h=1e-5):
    """Calculate the second order backward difference quotient of f at x."""

    return (3*f(x)-4*f(x-h)+f(x-2*h))/(2*h)

def cdq2(f, x, h=1e-5):
    """Calculate the second order centered difference quotient of f at x."""

    return (f(x+h)-f(x-h))/(2*h)

def cdq4(f, x, h=1e-5):
    """Calculate the fourth order centered difference quotient of f at x."""

    return (f(x-2*h)-8*f(x-h)+8*f(x+h)-f(x+2*h))/(12*h)


# Problem 3
def prob3(x0):
    """Let f(x) = (sin(x) + 1)^(sin(cos(x))). Use prob1() to calculate the
    exact value of f'(x0). Then use fdq1(), fdq2(), bdq1(), bdq2(), cdq1(),
    and cdq2() to approximate f'(x0) for h=10^-8, 10^-7, ..., 10^-1, 1.
    Track the absolute error for each trial, then plot the absolute error
    against h on a log-log scale.

    Parameters:
        x0 (float): The point where the derivative is being approximated.
    """
    x = sy.symbols('x')
    t = np.linspace(-np.pi, np.pi)
    f = sy.lambdify(x,(sy.sin(x)+1)**(sy.sin(sy.cos(x))))
    fprime = prob1()
    for1 = []
    for2 = []
    back1 = []
    back2 = []
    cen2 = []
    cen4 = []
    H = np.logspace(-8,0,9)
    for h in H:
        actual = fprime(x0)
        for1.append(abs(fdq1(f, x0, h) - actual))
        for2.append(abs(fdq2(f, x0, h) - actual))
        back1.append(abs(bdq1(f, x0, h) - actual))
        back2.append(abs(bdq2(f, x0, h) - actual))
        cen2.append(abs(cdq2(f, x0, h) - actual))
        cen4.append(abs(cdq4(f, x0, h) - actual))

    plt.loglog(H, for1, "o-b", label = 'Order 1 Forward')
    plt.loglog(H, for2, '*-k', alpha = .8, label = 'Order 2 Forward')
    plt.loglog(H, back1, '^-r', alpha = .6, label = 'Order 1 Backward')
    plt.loglog(H, back2, 'x-g', alpha = .4, label = 'Order 2 Backward')
    plt.loglog(H, cen2, 'p-c', alpha = .5, label = 'Order 2 Centered')
    plt.loglog(H, cen4, '+-m', label = 'Order 4 Centered')
    plt.legend(loc = 'best', fontsize = 9)
    plt.show()

# Problem 4
def prob4():
    """The radar stations A and B, separated by the distance 500m, track a
    plane C by recording the angles alpha and beta at one-second intervals.
    Your goal, back at air traffic control, is to determine the speed of the
    plane.

    Successive readings for alpha and beta at integer times t=7,8,...,14
    are stored in the file plane.npy. Each row in the array represents a
    different reading; the columns are the observation time t, the angle
    alpha (in degrees), and the angle beta (also in degrees), in that order.
    The Cartesian coordinates of the plane can be calculated from the angles
    alpha and beta as follows.

    x(alpha, beta) = a tan(beta) / (tan(beta) - tan(alpha))
    y(alpha, beta) = (a tan(beta) tan(alpha)) / (tan(beta) - tan(alpha))

    Load the data, convert alpha and beta to radians, then compute the
    coordinates x(t) and y(t) at each given t. Approximate x'(t) and y'(t)
    using a first order forward difference quotient for t=7, a first order
    backward difference quotient for t=14, and a second order centered
    difference quotient for t=8,9,...,13. Return the values of the speed at
    each t.
    """
    h = 1e-4
    plane = np.load('plane.npy')
    t = plane[:,0]
    alpha = plane[:,1]*np.pi/180
    beta = plane[:,2]*np.pi/180
    x = alpha*(np.tan(beta)/(np.tan(beta)-np.tan(alpha)))
    y = alpha*(np.tan(beta)*np.tan(alpha))/(np.tan(beta)-np.tan(alpha))
<<<<<<< HEAD

    print('t = 7','\t','x`=',x[1]-x[0],'\t','y`=',y[1]-y[0])

=======
    
    xprime = []
    yprime = []
    xprime.append(x[1]-x[0])
    yprime.append(y[1]-y[0])
        
>>>>>>> 503266e93098eb985e3428db4d8c2c28ed2821c5
    for h in range(1,7):
        xprime.append((x[h+1]-x[h-1])/2)
        yprime.append((y[h+1]-y[h-1])/2)

    xprime.append(x[7]-x[6])
    yprime.append(y[7]-y[6])
    
    return [np.sqrt((xprime[i])**2+(yprime[i])**2) for i in range(len(xprime))]

# Problem 5
def jacobian_cdq2(f, x, h=1e-5):
    """Approximate the Jacobian matrix of f:R^n->R^m at x using the second
    order centered difference quotient.

    Parameters:
        f (function): the multidimensional function to differentiate.
            Accepts a NumPy (n,) ndarray and returns an (m,) ndarray.
            For example, f(x,y) = [x+y, xy**2] could be implemented as follows.
            >>> f = lambda x: np.array([x[0] + x[1], x[0] * x[1]**2])
        x ((n,) ndarray): the point in R^n at which to compute the Jacobian.
        h (float): the step size in the finite difference quotient.

    Returns:
        ((m,n) ndarray) the Jacobian matrix of f at x.
    """

    # m = len(f)
    n = len(x)
    I = np.eye(n)
    jacob = np.array([np.array((f(x+h*I[j])-f(x-h*I[j]))/(2*h)) for j in range(n)]).T
    return jacob


# Problem 6
def cheb_poly(x, n):
    """Compute the nth Chebyshev polynomial at x.

    Parameters:
        x (autograd.ndarray): the points to evaluate T_n(x) at.
        n (int): The degree of the polynomial.
    """
    if n == 0:
        return anp.ones_like(x)
    T = [1, x]
    for i in range(2,n+1):
        T.append((2*x*T[i-1]-T[i-2]))

    return T[-1]

def prob6():
    """Use Autograd and cheb_poly() to create a function for the derivative
    of the Chebyshev polynomials, and use that function to plot the derivatives
    over the domain [-1,1] for n=0,1,2,3,4.
    """
<<<<<<< HEAD
    pass
    # x = anp.arange(-1.,1.,100)
    # print(x)
    # for n in range(2,5):
    #     print(n)
    #     if n == 0:
    #         ax = plt.subplot(2,3,1)
    #         ax.plot(x,anp.ones_like(x))
    #     x0 = sy.symbols('x0')
    #     T = [1, x0]
    #     T.append([2*x0*T[i-1]-T[i-2] for i in range(2,n+1)])
    #     print('T[-1]',T)
    #     T = sy.lambdify(x0,T[-1])
    #     print('T')
    #     dT = elementwise_grad(T)
    #     print('dT',dT)
    #     ax = plt.subplot(2,3,n)
    #     ax.plot(x, dT(x))

    # plt.show()


=======
    gradient = elementwise_grad(cheb_poly)
    domain = anp.linspace(-1,1)
    
    for n in range(5):
        # ax = plt.subplot(2,3,n+1)
        plt.plot(domain, gradient(domain,n), label = str('n = '+str(n)))
    
    plt.legend()
    plt.show()
    
    
>>>>>>> 503266e93098eb985e3428db4d8c2c28ed2821c5
# Problem 7
def prob7(N=200):
    """Let f(x) = (sin(x) + 1)^sin(cos(x)). Perform the following experiment N
    times:

        1. Choose a random value x0.
        2. Use prob1() to calculate the “exact” value of f′(x0). Time how long
            the entire process takes, including calling prob1() (each
            iteration).
        3. Time how long it takes to get an approximation of f'(x0) using
            cdq4(). Record the absolute error of the approximation.
        4. Time how long it takes to get an approximation of f'(x0) using
            Autograd (calling grad() every time). Record the absolute error of
            the approximation.

    Plot the computation times versus the absolute errors on a log-log plot
    with different colors for SymPy, the difference quotient, and Autograd.
    For SymPy, assume an absolute error of 1e-18.
    """
<<<<<<< HEAD
    raise NotImplementedError("Problem 7 Incomplete")


=======
    
    f = lambda x : (np.sin(x) + 1)**np.sin(np.cos(x))
    af = lambda x : (anp.sin(x) + 1)**anp.sin(anp.cos(x))
    prob1time = []
    prob1error = []
    cdq4time = []
    cdq4error = []
    gradtime = []
    graderror = []
    for n in range(N):
        x0 = random.random()
        start = time.perf_counter()
        exact = prob1()(x0)
        prob1time.append(time.perf_counter()-start)
        prob1error.append(1e-18)
    
        start = time.perf_counter()
        approx = cdq4(f, x0)
        cdq4time.append(time.perf_counter()-start)
        cdq4error.append(abs(exact - approx))
        
        start = time.perf_counter()
        approx = grad(af)(x0)
        gradtime.append(time.perf_counter()-start)
        graderror.append(abs(exact - approx))
        
    ax = plt.gca()
    ax.scatter(prob1time , prob1error , c='blue', alpha=0.25, edgecolors='none',
               label = 'SymPy')
    # ax.set_yscale('log')
    ax.set_xscale('log')
    
    ax.scatter(cdq4time , cdq4error , c='orange', alpha=0.25, edgecolors='none',
               label = 'Difference Quotients')
    ax.set_yscale('log')
    ax.set_xscale('log')
    
    ax.scatter(gradtime , graderror , c='green', alpha=0.25, edgecolors='none',
               label = 'Autograd')
    ax.set_yscale('log')
    ax.set_xscale('log')
    
    plt.xlabel('Computation Time (seconds)')
    plt.ylabel('Absolute Error')
    plt.legend()
        
>>>>>>> 503266e93098eb985e3428db4d8c2c28ed2821c5
if __name__ == '__main__':
    pass
    # x = sy.symbols('x')
    # fprime = prob1()
    # f = sy.lambdify(x,(sy.sin(x)+1)**(sy.sin(sy.cos(x))))
    # t = np.linspace(-np.pi, np.pi)
    # plt.plot(t, f(t))
    # plt.plot(t, fprime(t))
    # ax = plt.gca()
    # ax.spines["bottom"].set_position("zero")


    """Prob2 test"""
    # x = sy.symbols('x')
    # t = np.linspace(-np.pi, np.pi)
    # f = sy.lambdify(x,(sy.sin(x)+1)**(sy.sin(sy.cos(x))))
    # fprime = prob1()
    # ax1 = plt.subplot(231)
    # ax1.plot(t, fdq1(f,t))

    # ax2 = plt.subplot(232)
    # ax2.plot(t, fdq2(f,t))

    # ax3 = plt.subplot(233)
    # ax3.plot(t, bdq1(f,t))

    # ax4 = plt.subplot(234)
    # ax4.plot(t, bdq2(f,t))

    # ax5 = plt.subplot(235)
    # ax5.plot(t, cdq2(f,t))

    # ax6 = plt.subplot(236)
    # ax6.plot(t, cdq4(f,t))

    # plt.show()
    # plt.plot(t, fprime(t))
    # plt.show()
