# linear_transformations.py
"""Volume 1: Linear Transformations.
Broderik Craig
Section 3
21 September 2021
"""

from random import random
import numpy as np
from matplotlib import pyplot as plt
import time

def plothorse(data):
    horse=np.load("horse.npy")
    
    plt.subplot(1,2,1)
    plt.plot(data[0],data[1],'k,')
    plt.axis([-1,1,-1,1])
    plt.gca().set_aspect("equal")
    
    plt.subplot(1,2,2)
    plt.plot(horse[0],horse[1],'k,')
    plt.axis([-1,1,-1,1])
    plt.gca().set_aspect("equal")
    
    plt.show()

# Problem 1
def stretch(A, a, b):
    
    for i in range(len(A[0])): #Iterate through each x,y coordinate
        A[0][i]=a*A[0][i]       #multiply the x-coordinate by a
        A[1][i]=b*A[1][i]       #multiply the y-coordinate by y
        
    return A
    

    """Scale the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """



def shear(A, a, b):
    """Slant the points in A by a in the x direction and b in the
    y direction.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): scaling factor in the x direction.
        b (float): scaling factor in the y direction.
    """
    transformation=np.array([1,a,b,1]).reshape(2,2) #Define the transformation matrix
    
    for i in range(len(A[0])):
        vector=[A[0][i],A[1][i]]  #Define a vector of a given (x,y)
        newvector=transformation.dot(vector)    #define a new vector as a product of the (x,y) and the transformation matrix
        A[0][i]=newvector[0]    #Redefine the x-coordinate
        A[1][i]=newvector[1]    #Redefine the y-coordinate
    
    return A
    

def reflect(A, a, b):
    """Reflect the points in A about the line that passes through the origin
    and the point (a,b).

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        a (float): x-coordinate of a point on the reflecting line.
        b (float): y-coordinate of the same point on the reflecting line.
    """
    
    transformation=np.array([(a**2-b**2)/(a**2+b**2),(2*a*b)/(a**2+b**2),(2*a*b)/(a**2+b**2),(b**2-a**2)/(a**2+b**2)]).reshape(2,2)
    
    for i in range(len(A[0])):
        vector=[A[0][i],A[1][i]]  #Define a vector of a given (x,y)
        newvector=transformation.dot(vector)    #define a new vector as a product of the (x,y) and the transformation matrix
        A[0][i]=newvector[0]    #Redefine the x-coordinate
        A[1][i]=newvector[1]    #Redefine the y-coordinate
    return A

def rotate(A, theta):
    """Rotate the points in A about the origin by theta radians.

    Parameters:
        A ((2,n) ndarray): Array containing points in R2 stored as columns.
        theta (float): The rotation angle in radians.
    """
    A=np.array(A)
    transformation=np.array([np.cos(theta),-np.sin(theta),np.sin(theta),np.cos(theta)]).reshape(2,2)
    return transformation @ A

# Problem 2
def solar_system(T, x_e, x_m, omega_e, omega_m):
    """Plot the trajectories of the earth and moon over the time interval [0,T]
    assuming the initial position of the earth is (x_e,0) and the initial
    position of the moon is (x_m,0).

    Parameters:
        T (int): The final time.
        x_e (float): The earth's initial x coordinate.
        x_m (float): The moon's initial x coordinate.
        omega_e (float): The earth's angular velocity.
        omega_m (float): The moon's angular velocity.
    """
    timespec=np.linspace(0,T,1000)
    moon=np.array([[(x_m),0]]).T
    earth=np.array([[x_e,0]]).T
    for t in timespec:
        newearth=rotate([x_e,0],t*omega_e)
        earth=np.column_stack((earth,newearth))
        moon=np.column_stack((moon,rotate([x_m - x_e,0],t*omega_m)+newearth))
        
    plt.plot(earth[0],earth[1]) #,moon[0],moon[1])
    plt.plot(moon[0], moon[1])
    # plt.axis([-450,450,-450,450])
    plt.gca().set_aspect("equal")

    plt.show()
    
    

def random_vector(n):
    """Generate a random vector of length n as a list."""
    return [random() for i in range(n)]

def random_matrix(n):
    """Generate a random nxn matrix as a list of lists."""
    return [[random() for j in range(n)] for i in range(n)]

def matrix_vector_product(A, x):
    """Compute the matrix-vector product Ax as a list."""
    m, n = len(A), len(x)
    return [sum([A[i][k] * x[k] for k in range(n)]) for i in range(m)]

def matrix_matrix_product(A, B):
    """Compute the matrix-matrix product AB as a list of lists."""
    m, n, p = len(A), len(B), len(B[0])
    return [[sum([A[i][k] * B[k][j] for k in range(n)])
                                    for j in range(p) ]
                                    for i in range(m) ]

# Problem 3
def prob3():
    """Use time.time(), timeit.timeit(), or %timeit to time
    matrix_vector_product() and matrix-matrix-mult() with increasingly large
    inputs. Generate the inputs A, x, and B with random_matrix() and
    random_vector() (so each input will be nxn or nx1).
    Only time the multiplication functions, not the generating functions.

    Report your findings in a single figure with two subplots: one with matrix-
    vector times, and one with matrix-matrix times. Choose a domain for n so
    that your figure accurately describes the growth, but avoid values of n
    that lead to execution times of more than 1 minute.
    """
    # n=1
    xaxis=[]
    timevec=[]
    timemat=[]
    start=time.perf_counter()
    for n in range(1,250): 
    # while True:
        # print(n)
        A=np.random.rand(n,n)
        B=np.random.rand(n,n)
        x=np.random.rand(n,1)

        startvec=time.perf_counter()
        matrix_vector_product(A,x)
        endvec=time.perf_counter()
        timevec.append(endvec-startvec) 
        startmat=time.perf_counter()
        matrix_matrix_product(A, B)
        endmat=time.perf_counter()
        if endmat-startmat > 15:
            timemat.append(endmat-startmat)
            break
        timemat.append(endmat-startmat)
        # print(endmat-startmat)
        xaxis.append(n)
        if time.perf_counter()-start>50:
            break
    
    ax1=plt.subplot(1,2,1)
    ax1.plot(xaxis,timevec)
    ax1.set_title("Vector Multiplication")
    
    ax2=plt.subplot(1,2,2)
    ax2.plot(xaxis,timemat)
    ax2.set_title("Matrix Multiplication")
    
    ax2.set_xlabel("Matrix Dimension")
    ax2.set_ylabel("time")
    
    plt.tight_layout()
    plt.show()
    print(time.perf_counter() - start)

# Problem 4
def prob4():
    """Time matrix_vector_product(), matrix_matrix_product(), and np.dot().

    Report your findings in a single figure with two subplots: one with all
    four sets of execution times on a regular linear scale, and one with all
    four sets of exections times on a log-log scale.
    """
    domain=2**np.arange(1,9)
    timevec1=[]
    timemat1=[]
    timevec2=[]
    timemat2=[]
    xval=[]
    start=time.perf_counter()
    for n in domain: 
        boo = False
        A=np.random.rand(n,n)
        B=np.random.rand(n,n)
        x=np.random.rand(n,1)

        startvec=time.perf_counter()
        matrix_vector_product(A,x)
        endvec=time.perf_counter()
        timevec1.append(endvec-startvec) 
        startmat=time.perf_counter()
        matrix_matrix_product(A, B)
        endmat=time.perf_counter()
        if endmat-startmat > 20:
            # timemat1.append(endmat-startmat)
            boo = True
        timemat1.append(endmat-startmat)
        # print(endmat-startmat)

        # n=2**n
        A=np.random.rand(n,n)
        B=np.random.rand(n,n)
        x=np.random.rand(n,1)
        startvec=time.perf_counter()
        A.dot(x)
        endvec=time.perf_counter()
        timevec2.append(endvec-startvec) 
        startmat=time.perf_counter()
        A.dot(B)
        endmat=time.perf_counter()
        # if endmat-startmat >1:
        #     timemat.append(endmat-startmat)
        #     boo==True
        timemat2.append(endmat-startmat)
        # print(endmat-startmat)
        # n+=1
        xval.append(n)
        if time.perf_counter()-start>50:
            break
        if boo == True:
            break
    
    ax1=plt.subplot(1,2,1)
    ax1.plot(xval,timevec1,'b.-',lw=2,ms=5,label="Matrix-Vector-product")
    ax1.plot(xval,timemat1,'r.-',lw=2,ms=5,label="Matrix-Matrix-product")
    ax1.plot(xval,timevec2,'g.-',lw=2,ms=5,label="Matrix-Vector-dot")
    ax1.plot(xval,timemat2,'k.-',lw=2,ms=5,label="Matrix-Matrix-dot")
    ax1.set_title("Linear Time")
    ax1.legend(loc="upper left")
    # ax1.set_aspect("equal")
    
    ax2=plt.subplot(1,2,2)
    ax2.loglog(xval,timevec1,'b.-',lw=2,ms=5)
    ax2.loglog(xval,timemat1,'r.-',lw=2,ms=5)
    ax2.loglog(xval,timevec2,'g.-',lw=2,ms=5)
    ax2.loglog(xval,timemat2,'k.-',lw=2,ms=5)
    ax2.set_title("Log Time")
    # ax2.set_aspect("equal")
    
    ax2.set_xlabel("Matrix Dimension")
    ax2.set_ylabel("time")
    
    # plt.legend(loc = 'lower left')
    plt.tight_layout()
    plt.show()