# matplotlib_intro.py
"""Python Essentials: Intro to Matplotlib.
Broderik S. Craig
MATH 345
9/14/21
"""
import numpy as np
from matplotlib import pyplot as plt

# Problem 1
def var_of_means(n):
    """Construct a random matrix A with values drawn from the standard normal
    distribution. Calculate the mean value of each row, then calculate the
    variance of these means. Return the variance.

    Parameters:
        n (int): The number of rows and columns in the matrix A.

    Returns:
        (float) The variance of the means of each row.
    """

    A = np.random.normal(size=(n,n))   #Create random normal distribution matrix of size nxn
    mean=np.mean(A,axis=0)             #Take the mean of each row (axis=0 instructs the function to take the mean of the rows)
    return np.var(mean)                #Returns a vector of the means of each row

def prob1():
    """Create an array of the results of var_of_means() with inputs
    n = 100, 200, ..., 1000. Plot and show the resulting array.
    """
    results=np.array([])             #set an empty array
    for n in range(100, 1000, 100):  #loop 100-1000 stepping by 100
        variance=var_of_means(n)     #Call the var_of_mens function
        results=np.append(results,variance)   #Add variance of the next stop to the array
    plt.plot(results)                #Plot the results
    plt.show()
    return results

# Problem 2
def prob2():
    """Plot the functions sin(x), cos(x), and arctan(x) on the domain
    [-2pi, 2pi]. Make sure the domain is refined enough to produce a figure
    with good resolution.
    """
    x=np.linspace(-2*np.pi,2*np.pi)
    y1=np.sin(x)
    y2=np.cos(x)
    y3=np.arctan(x)
    plt.plot(x,y1,'b')
    plt.plot(x,y2,'r')
    plt.plot(x,y3,'k')
    
    return plt.show()
    

# Problem 3
def prob3():
    """Plot the curve f(x) = 1/(x-1) on the domain [-2,6].
        1. Split the domain so that the curve looks discontinuous.
        2. Plot both curves with a thick, dashed magenta line.
        3. Set the range of the x-axis to [-2,6] and the range of the
           y-axis to [-6,6].
    """
    x=[np.linspace(-2,1)[:-1],np.linspace(1,6)[1:]]
    f_x0=1/(x[0]-1)
    f_x1=1/(x[1]-1)
    plt.plot(x[0],f_x0,'--m',linewidth=4)
    plt.plot(x[1],f_x1, '--m',linewidth=4)
    plt.xlim([-2,6])
    plt.ylim([-6,6])
    plt.show()

# Problem 4
def prob4():
    """Plot the functions sin(x), sin(2x), 2sin(x), and 2sin(2x) on the
    domain [0, 2pi].
        1. Arrange the plots in a square grid of four subplots.
        2. Set the limits of each subplot to [0, 2pi]x[-2, 2].
        3. Give each subplot an appropriate title.
        4. Give the overall figure a title.
        5. Use the following line colors and styles.
              sin(x): green solid line.
             sin(2x): red dashed line.
             2sin(x): blue dashed line.
            2sin(2x): magenta dotted line.
    """
    x=np.linspace(0,2*np.pi)
    plt.suptitle('Sine functions')
    plt.axis([0,2*np.pi,-2,2])
    
    ax1=plt.subplot(2,2,1)
    ax1.plot(x, np.sin(x), '-g')
    plt.ylim([-2,2])
    plt.title('Sin(x)')
    

    ax2=plt.subplot(2,2,2)
    ax2.plot(x, np.sin(2*x),'--r')
    plt.ylim([-2,2])
    plt.title('Sin(2x)')
    
    ax3=plt.subplot(2,2,3)
    ax3.plot(x,2*np.sin(x),'--b')
    plt.title('2Sin(x)')
    
    ax3=plt.subplot(2,2,4)
    ax3.plot(x,2*np.sin(2*x),':m')
    plt.title('2Sin(2x)')
    
    plt.subplots_adjust(wspace=.2, hspace=.5)
    plt.show()
    


# Problem 5
def prob5():
    data=np.load('./FARS.npy')
    # print(data)
    x=data[:,1]
    y=data[:,2]
    ax1=plt.subplot(1,2,1)
    ax1.plot(x,y,'k,')
    ax1.set_aspect("equal")

    ax2=plt.subplot(1,2,2)
    ax2.hist(data,bins=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24])
    ax2.set_xlabel("Hour of Fatality")
    ax2.set_ylabel("Number of Fatalities")
    ax2.xlim=([0,23])
    plt.subplots_adjust(wspace=.5, hspace=.5)

    
    plt.show()
    # plt.plot(data,'o',lw=.1)
    """Visualize the data in FARS.npy. Use np.load() to load the data, then
    create a single figure with two subplots:
        1. A scatter plot of longitudes against latitudes. Because of the
            large number of data points, use black pixel markers (use "k,"
            as the third argument to plt.plot()). Label both axes.
        2. A histogram of the hours of the day, with one bin per hour.
            Label and set the limits of the x-axis.
    """


# Problem 6
def prob6():
    """Plot the function f(x,y) = sin(x)sin(y)/xy on the domain
    [-2pi, 2pi]x[-2pi, 2pi].
        1. Create 2 subplots: one with a heat map of f, and one with a contour
            map of f. Choose an appropriate number of level curves, or specify
            the curves yourself.
        2. Set the limits of each subplot to [-2pi, 2pi]x[-2pi, 2pi].
        3. Choose a non-default color scheme.
        4. Add a colorbar to each subplot.
    """
    x=np.linspace(-2*np.pi,2*np.pi)
    y=np.linspace(-2*np.pi,2*np.pi)
    X, Y = np.meshgrid(x,y)

    g=np.sin(X)*np.sin(Y)/(X*Y)
    
    plt.subplot(121)
    plt.pcolormesh(X,Y,g, cmap="magma",shading='auto')
    plt.xlim(-2*np.pi,2*np.pi)
    plt.ylim(-2*np.pi,2*np.pi)
    plt.colorbar()
    
    plt.subplot(122) 
    plt.contour(X,Y,g, 100, cmap='viridis')   
    plt.xlim(-2*np.pi,2*np.pi)
    plt.ylim(-2*np.pi,2*np.pi)
    plt.colorbar()
    
    plt.subplots_adjust(wspace=.3, hspace=.5)
    
    plt.show()
    
    
    
    
