# sympy_intro.py
"""Python Essentials: Introduction to SymPy.
<Name>
<Class>
<Date>
"""
import sympy as sy
from matplotlib import pyplot as plt
import numpy as np

# Problem 1
def prob1():
    """Return an expression for

        (2/5)e^(x^2 - y)cosh(x+y) + (3/7)log(xy + 1).

    Make sure that the fractions remain symbolic.
    """

    x, y = sy.symbols('x, y')
    return sy.Rational(2, 5)*sy.exp(x**2 - y)*sy.cosh(x + y) + sy.Rational(3, 7)*sy.log(x*y + 1)
    


# Problem 2
def prob2():
    """Compute and simplify the following expression.

        product_(i=1 to 5)[ sum_(j=i to 5)[j(sin(x) + cos(x))] ]
    """
    x = sy.symbols('x')
    i = sy.symbols('i')
    j = sy.symbols('j')
    sumthing = sy.summation(j*(sy.sin(x)+sy.cos(x)), (j,i,5))
    return sy.product(sumthing , (i,1,5))


# Problem 3
def prob3(N):
    """Define an expression for the Maclaurin series of e^x up to order N.
    Substitute in -y^2 for x to get a truncated Maclaurin series of e^(-y^2).
    Lambdify the resulting expression and plot the series on the domain
    y in [-3,3]. Plot e^(-y^2) over the same domain for comparison.
    """
    x, y, n = sy.symbols('x y n')
    e_x = sy.summation((x**n)/sy.factorial(n) ,(n, 0, N))  #sum(x**n/n!)_n=1**N
    ey2 = sy.lambdify(y, e_x.subs(x, -y**2))               # substitute -y**2, lambdify
    
    t = np.linspace(-2,2)
    plt.plot(t, ey2(t), '--k', label = 'approximation')
    plt.plot(t, np.exp(-t**2), label = 'actual')
    plt.legend()
    plt.title('e^(-y^2)')
    plt.show()
    
# Problem 4
def prob4():
    """The following equation represents a rose curve in cartesian coordinates.

    0 = 1 - [(x^2 + y^2)^(7/2) + 18x^5 y - 60x^3 y^3 + 18x y^5] / (x^2 + y^2)^3

    Construct an expression for the nonzero side of the equation and convert
    it to polar coordinates. Simplify the result, then solve it for r.
    Lambdify a solution and use it to plot x against y for theta in [0, 2pi].
    """
    x, y, r, theta = sy.symbols('x, y, r, theta')
    top = (x**2 + y**2)**sy.Rational(7,2) + 18*y*(x**5) - 60*(x**3)*(y**3) + 18*x*(y**5)
    bottom = (x**2 + y**2)**3
    cart = 1 - top/bottom
    polar = cart.subs({x:r*sy.cos(theta), y:r*sy.sin(theta)})
    polar = sy.simplify(polar)
    rval = sy.solve(polar, r)
    rose = sy.lambdify(theta, rval[0])
    t = np.linspace(0, 2*np.pi, 1000)
    plt.plot(rose(t)*np.cos(t), rose(t)*np.sin(t))
    

# Problem 5
def prob5():
    """Calculate the eigenvalues and eigenvectors of the following matrix.

            [x-y,   x,   0]
        A = [  x, x-y,   x]
            [  0,   x, x-y]

    Returns:
        (dict): a dictionary mapping eigenvalues (as expressions) to the
            corresponding eigenvectors (as SymPy matrices).
    """
    x, y, z, lam = sy.symbols('x y z lam')
    A = sy.Matrix([ [x-y, x,   0],
                    [x,   x-y, x],
                    [0,   x,   x-y] ])

    
    eig = sy.solve(sy.det(A - lam*sy.eye(3)), lam)
    eigvec = {}
    for e in eig:
        eigvec[e] = ((A - e*sy.eye(3,3)).nullspace())
    return eigvec
    
# Problem 6
def prob6():
    """Consider the following polynomial.

        p(x) = 2*x^6 - 51*x^4 + 48*x^3 + 312*x^2 - 576*x - 100

    Plot the polynomial and its critical points. Determine which points are
    maxima and which are minima.

    Returns:
        (set): the local minima.
        (set): the local maxima.
    """
    x = sy.symbols('x')
    p = 2*x**6 - 51*x**4 + 48*x**3 + 312*x**2 - 576*x - 100
    dp = sy.diff(p,x)
    critical = sy.solve(dp, x)
    ddp = sy.diff(dp,x)
    ddp = sy.lambdify(x, ddp)
    positive = []
    negative = []
    for point in critical:
        if ddp(point)>0:
            positive.append(point)
        elif ddp(point)<0:
            negative.append(point)
        else:
            raise ValueError('Inconclusive critical point at', point)
    p = sy.lambdify(x, p)
    
    t = np.linspace(-5,5)
    plt.scatter(positive, [p(point) for point in positive], label = 'Minima')
    plt.scatter(negative, [p(point) for point in negative], color = 'r', label = 'Maxima')
    plt.ylim([-1000,1500])
    plt.legend()
    plt.plot(t, p(t), color = 'k')

# Problem 7
def prob7(r = 2):
    """Calculate the integral of f(x,y,z) = (x^2 + y^2 + z^2)^2 over the
    sphere of radius r. Lambdify the resulting expression and plot the integral
    value for r in [0,3]. Return the value of the integral when r = 2.

    Returns:
        (float): the integral of f over the sphere of radius 2.
    """
    
    x, y, z, rho, theta, phi = sy.symbols('x y z rho theta phi')
    f = (x**2+y**2+z**2)**2
    
    h1 = rho*sy.sin(phi)*sy.cos(theta)
    h2 = rho*sy.sin(phi)*sy.sin(theta)
    h3 = rho*sy.cos(phi)
    
    h = sy.Matrix([[rho*sy.sin(phi)*sy.cos(theta)],[rho*sy.sin(phi)*sy.sin(theta)],[rho*sy.cos(phi)]])
    J = h.jacobian([rho,theta,phi])

    f = f.subs({x:h1, y:h2, z:h3})
    
    integral = sy.integrate(
        sy.integrate(
        sy.integrate(
            f*-sy.det(J),
            (rho,0,r)),
        (theta,0,2*sy.pi)),
        (phi,0,sy.pi))
    return float(integral)


